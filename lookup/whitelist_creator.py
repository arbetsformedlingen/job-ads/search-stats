from lookup import additional_words, taxonomy_words, company_names, companies_sthlm_exchange, stopwords, \
    whitelisted_words, schools
from lookup import word_stems
from search_trends import file_handler


def create_whitelist():
    words = []
    for word_list in [stopwords.swe,
                      stopwords.eng,
                      word_stems.words,
                      additional_words.words,
                      taxonomy_words.words,
                      companies_sthlm_exchange.words,
                      company_names.words,
                      whitelisted_words.words,
                      schools.words]:
        words.extend(word_list)

    full_whitelist = file_handler.unpickle_whitelist() | set(words)  # union

    return full_whitelist
