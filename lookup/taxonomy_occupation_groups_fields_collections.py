
occupation_collections = {
    "ja7J_P8X_YC9": "Bygg",
    "8too_bEs_7NU": "Kultur",
    "XouD_cCj_HZN": "Sjöfart",
    "UdVa_jRr_9DE": "Yrkessamling, yrken utan krav på utbildning",
    "epLc_SkD_cbT": "Yrkessamling, chefsyrken"
}

occupation_groups = {"Fghp_zje_WA8": "Fastighetsmäklare", "U8HK_Jdi_GuC": "Fastighetsförvaltare",
                     "ieVp_d5L_rS9": "Övriga servicearbetare", "9DdK_AZY_ZNK": "Vaktmästare m.fl.",
                     "qhbP_JEJ_HtK": "Reklamutdelare och tidningsdistributörer",
                     "X7HR_rZK_4wj": "Renhållnings- och återvinningsarbetare",
                     "xyW2_toA_Skh": "Torg- och marknadsförsäljare m.fl.",
                     "1wqU_Jhj_amb": "Kafé- och konditoribiträden",
                     "tPox_ie4_X9X": "Restaurang- och köksbiträden m.fl.", "a8gg_ktU_w8V": "Pizzabagare m.fl.",
                     "bwnk_nv2_1cR": "Ramppersonal, flyttkarlar och varupåfyllare m.fl.",
                     "SZ7Q_2zF_nFy": "Hamnarbetare", "DxiF_hTo_b3X": "Handpaketerare och andra fabriksarbetare",
                     "iHrC_aRv_fVm": "Grovarbetare inom bygg och anläggning",
                     "1EBv_UzM_ozQ": "Bärplockare och plantörer m.fl.",
                     "FjdF_H2e_tAG": "Bilrekonditionerare, fönsterputsare m.fl.",
                     "o1gn_FoS_fB7": "Övrig hemservicepersonal m.fl.", "Hi9c_iTe_gHH": "Barnskötare",
                     "Z6TY_xDf_Yup": "Städare", "CbQK_7th_Tyf": "Matroser och jungmän m.fl.",
                     "MrcX_RDn_N53": "Truckförare", "XYzP_5Tn_7Ep": "Kranförare m.fl.",
                     "3ZtN_hhA_SM7": "Anläggningsmaskinförare m.fl.",
                     "KwVo_sQu_G2e": "Förare av jordbruks- och skogsmaskiner",
                     "3MBw_pDA_P2F": "Lastbilsförare m.fl.",
                     "FiDm_2S8_gx7": "Buss- och spårvagnsförare",
                     "ffz2_2n3_R7g": "Taxiförare m.fl.",
                     "P7Ay_ooF_pri": "Övriga bil-, motorcykel- och cykelförare",
                     "nk6Z_5oD_QoM": "Bangårdspersonal",
                     "UGgc_Vm9_Bog": "Lokförare", "shkw_ojo_UL2": "Fordonsmontörer",
                     "SgAJ_DW2_FN5": "Montörer, elektrisk och elektronisk utrustning",
                     "Kxm9_N3V_shF": "Montörer, metall-, gummi- och plastprodukter",
                     "8ddv_P8e_iYz": "Montörer, träprodukter",
                     "HXzD_qvt_HV6": "Övriga montörer",
                     "Cppd_9P7_HeR": "Drifttekniker vid värme- och vattenverk",
                     "QEBd_qHu_vGr": "Processövervakare, kemisk industri",
                     "E87t_egE_pjA": "Processövervakare, metallproduktion",
                     "7Zqy_hSM_Fig": "Övriga drifttekniker och processövervakare",
                     "aU9p_ekE_Fqf": "Maskinoperatörer, påfyllning, packning och märkning",
                     "hxm6_xkr_dHm": "Övriga process- och maskinoperatörer",
                     "YKL2_FCB_1yr": "Butikssäljare, fackhandel",
                     "AUs8_M5f_dLt": "Maskinoperatörer inom ytbehandling, trä",
                     "v7W3_cLQ_q2S": "Manuella ytbehandlare, trä",
                     "UdrS_qzV_P7i": "Operatörer inom sågverk, hyvleri och plywood m.m.",
                     "1G6H_AuG_qyy": "Processoperatörer, papper", "4obu_LL6_RqJ": "Processoperatörer, pappersmassa",
                     "gJtZ_EBc_n2z": "Maskinoperatörer, kött- och fiskberedningsindustri",
                     "yJk6_azD_ywF": "Maskinoperatörer, mejeri",
                     "kF59_fge_kts": "Maskinoperatörer, kvarn-, bageri- och konfektyrindustri",
                     "icae_Y69_H8E": "Övriga maskinoperatörer, livsmedelsindustri m.m.",
                     "4DET_dLf_iB9": "Maskinoperatörer, blekning, färgning och tvättning",
                     "skLS_kpa_Ui1": "Övriga maskinoperatörer, textil-, skinn- och läderindustri",
                     "ENV6_BNa_brP": "Maskinoperatörer, gummiindustri",
                     "yPQ4_EaN_Dtj": "Maskinoperatörer, plastindustri",
                     "iZLB_KpD_LLo": "Maskinoperatörer, pappersvaruindustri",
                     "Vfum_e6B_7Bw": "Maskinoperatörer, kemisktekniska och fotografiska produkter",
                     "rxmK_wwM_1NA": "Maskinoperatörer, farmaceutiska produkter",
                     "UcNa_inr_snV": "Övriga maskin- och processoperatörer vid stål- och metallverk",
                     "YiQw_N87_zLU": "Maskinoperatörer, ytbehandling", "m8TS_hdk_pSf": "Valsverksoperatörer",
                     "f71c_gjL_RDq": "Gruv- och stenbrottsarbetare",
                     "LEvM_7Hs_w8i": "Processoperatörer, stenkross- och malmförädling",
                     "7zKm_4NW_eey": "Brunnsborrare m.fl.",
                     "o31g_CX1_qhQ": "Maskinoperatörer, cement-, sten- och betongvaror",
                     "sX9n_CWa_KVC": "Bergsprängare", "vCBT_Pnd_8UB": "Stenhuggare m.fl.",
                     "QqHf_XKZ_eyy": "Slaktare och styckare m.fl.", "5qT8_z9d_8rw": "Bagare och konditorer",
                     "dAu7_VyL_hsy": "Provsmakare och kvalitetsbedömare",
                     "mwVN_oEH_g9k": "Skräddare och ateljésömmerskor m.fl.", "UZBj_yDX_Ctp": "Sömmare",
                     "vhUJ_LyD_XWc": "Tapetserare", "vJQk_YvC_XB7": "Läderhantverkare och skomakare",
                     "1Nwn_GYH_wkH": "Fin-, inrednings- och möbelsnickare",
                     "VKKg_e4t_t46": "Maskinsnickare och maskinoperatörer, träindustri",
                     "fWrt_sLg_koW": "Elektronikreparatörer och kommunikationselektriker m.fl.",
                     "MW1P_GNJ_4F9": "Distributionselektriker", "a75S_eLg_JWH": "Industrielektriker",
                     "yyF7_jsW_EeP": "Installations- och serviceelektriker", "LeQV_umf_wUS": "Prepresstekniker",
                     "XeJB_bkn_mRn": "Tryckare", "mPzg_93F_QEv": "Bokbindare m.fl.", "e6nk_AGp_y28": "Finmekaniker",
                     "MSnK_3Z8_4qA": "Verktygsmakare", "nwSm_phw_sPo": "Guld- och silversmeder",
                     "aKdV_u7V_R5j": "Musikinstrumentmakare och övriga konsthantverkare",
                     "Lzpv_ofL_3H5": "Motorfordonsmekaniker och fordonsreparatörer",
                     "iGTu_bYL_2T7": "Flygmekaniker m.fl.", "KttZ_Skc_bMR": "Underhållsmekaniker och maskinreparatörer",
                     "q8Ho_3Xw_sHq": "Smeder", "KYmy_iqC_xiQ": "Uppfödare och skötare av lantbrukets husdjur",
                     "vaEp_iBS_c5h": "Maskinställare och maskinoperatörer, metallarbete",
                     "4bxV_58E_P9N": "Slipare m.fl.", "J9Vz_JxJ_Bji": "Gjutare",
                     "wQ1E_Bzk_ZyA": "Svetsare och gasskärare", "bXfo_4hb_SXx": "Byggnads- och ventilationsplåtslagare",
                     "L4hA_SJP_SFi": "Tunnplåtslagare",
                     "TrwQ_RXT_W8a": "Stålkonstruktionsmontörer och grovplåtsslagare", "QKvX_v7r_PNL": "Målare",
                     "dKUb_KBP_mw3": "Lackerare och industrimålare", "SUfj_4D9_FNx": "Skorstensfejare",
                     "idjo_2Wr_a3E": "Saneringsarbetare m.fl.", "3g5x_Tm7_8QH": "Takmontörer",
                     "V79C_DD2_3Gj": "Golvläggare", "cGxX_R9c_kW1": "Isoleringsmontörer",
                     "J17g_Q2a_2u1": "Glastekniker", "fLKb_bJ3_69p": "VVS-montörer m.fl.",
                     "3UzE_P8A_9sS": "Kyl- och värmepumpstekniker m.fl.", "9mJN_VwW_Xjg": "Träarbetare, snickare m.fl.",
                     "NnB3_eWH_auo": "Murare m.fl.", "q2Ge_zrQ_zJb": "Betongarbetare",
                     "bZ4J_riZ_zK6": "Anläggningsarbetare", "DPPw_4wa_AsH": "Anläggningsdykare",
                     "Y4j1_P17_wVA": "Ställningsbyggare", "AD3U_cFH_rbe": "Övriga byggnads- och anläggningsarbetare",
                     "Lm3Z_gek_GYc": "Fiskare", "c8r8_e4L_gwj": "Fiskodlare", "qouP_wtb_93q": "Skogsarbetare",
                     "RYCQ_ogb_uUY": "Växtodlare och djuruppfödare, blandad drift",
                     "MSzH_F2n_Emb": "Uppfödare och skötare av sällskapsdjur",
                     "Lrsj_wRZ_sFV": "Övriga djuruppfödare och djurskötare",
                     "k3R3_UkC_nK2": "Odlare av jordbruksväxter, frukt och bär", "2xQ4_4kh_kTZ": "Trädgårdsodlare",
                     "XBh3_Xrm_C4R": "Trädgårdsanläggare m.fl.", "ug5y_o6z_U3L": "Brandmän",
                     "hgAg_gLr_YGg": "Kriminalvårdare", "iqZh_3yX_i8n": "Väktare och ordningsvakter",
                     "5dxv_nVQ_N8o": "Tandsköterskor", "4F9o_Jzv_Pwz": "Tandhygienister", "7fNG_sve_JWU": "Skötare",
                     "7pz9_eY9_D2s": "Vårdare, boendestödjare", "sq3e_WVv_Fjd": "Personliga assistenter",
                     "GiNX_ESA_AL1": "Övrig vård- och omsorgspersonal", "tAJS_JNb_hDH": "Vårdbiträden",
                     "3H17_ZZr_BEc": "Ambulanssjukvårdare", "kHgb_AHf_vrY": "Barnsköterskor",
                     "XW9L_tA9_WaX": "Elevassistenter m.fl.",
                     "NCeZ_rSk_B4D": "Eventsäljare och butiksdemonstratörer m.fl.",
                     "PxXc_16t_oZo": "Telefonförsäljare m.fl.", "WNVp_RYe_zLX": "Kassapersonal m.fl.",
                     "Fp7x_Yqt_bx9": "Säljande butikschefer och avdelningschefer i butik",
                     "s1Qk_o98_2o1": "Butikssäljare, dagligvaror", "AQrL_tTu_Wbg": "Optikerassistenter",
                     "JnTF_wXr_6Eh": "Bensinstationspersonal", "M47D_MU2_dhS": "Uthyrare",
                     "41Az_ioZ_rmH": "Apotekstekniker", "DSGC_nw7_WnH": "Övrig servicepersonal",
                     "R8bW_rsJ_b2G": "Begravnings- och krematoriepersonal", "df9D_e6y_61z": "Fastighetsskötare",
                     "FtyZ_Joo_tB8": "Städledare och husfruar", "LyBp_bY6_bZK": "Frisörer",
                     "xr7w_N6Q_QMA": "Hudterapeuter", "uxHV_KXc_PKD": "Massörer och massageterapeuter",
                     "HATH_AfN_RRz": "Fotterapeuter", "ezKk_kss_CiQ": "Övriga skönhets- och kroppsterapeuter",
                     "F19Z_oB6_pg3": "Terapeuter inom alternativmedicin", "XFXX_Jq9_Y2a": "Bartendrar",
                     "UjoW_meY_Zwt": "Hovmästare och servitörer", "BStc_SJh_DKG": "Kockar och kallskänkor",
                     "A6X3_efC_VNy": "Guider och reseledare", "s78G_Zp2_dkq": "Tågvärdar och ombordansvariga m.fl.",
                     "1Tso_4TB_RkE": "Kabinpersonal m.fl.", "gaBh_1vR_tNE": "Brevbärare och postterminalarbetare",
                     "TQkt_eeK_eNp": "Arkiv- och biblioteksassistenter m.fl.",
                     "957L_kZK_rHZ": "Transportledare och transportsamordnare",
                     "kLyY_rwr_aJr": "Lager- och terminalpersonal",
                     "WhBn_FAp_ZNW": "Arbetsledare inom lager och terminal",
                     "83wV_GJa_vMd": "Marknadsundersökare och intervjuare", "ByrL_ANp_UwV": "Kontorsreceptionister",
                     "bYde_Frc_xcB": "Hotellreceptionister m.fl.", "4aPu_2nd_8X6": "Telefonister",
                     "kZwz_D8K_bKs": "SOS-operatörer m.fl.", "ayQ4_G3W_A2w": "Övrig bevaknings- och säkerhetspersonal",
                     "pwRH_MT1_8nR": "Kundtjänstpersonal", "fdLf_oPp_wVw": "Resesäljare och trafikassistenter m.fl.",
                     "kDnq_Q98_sD3": "Fartygsbefäl m.fl.", "zKvJ_GnC_Szx": "Domstols- och juristsekreterare m.fl.",
                     "JAFv_ZaP_tDG": "Inkasserare och pantlånare m.fl.",
                     "YHwG_fk2_MDL": "Croupierer och oddssättare m.fl.",
                     "eQ4M_CNm_ozj": "Övriga kontorsassistenter och sekreterare",
                     "2kLc_pto_DpV": "Medicinska sekreterare, vårdadministratörer m.fl.",
                     "ZQTR_hVC_QhJ": "Skolassistenter m.fl.", "iWcY_mko_rq2": "Inköps- och orderassistenter",
                     "p16X_44f_rwZ": "Marknads- och försäljningsassistenter",
                     "MGk3_LvL_AU2": "Backofficepersonal m.fl.", "L7eh_ihX_vM5": "Löne- och personaladministratörer",
                     "ij8k_EwC_zyB": "Ekonomiassistenter m.fl.", "4BsK_q2C_sCL": "Ljus-, ljud- och scentekniker",
                     "CkRH_4Qn_iX3": "Bild- och sändningstekniker",
                     "Fv7d_YhP_YmS": "Webbmaster och webbadministratörer",
                     "VCpu_5EN_bBt": "Nätverks- och systemtekniker m.fl.", "MYAz_x9m_2LJ": "Systemadministratörer",
                     "hmaC_cfi_UKg": "Supporttekniker, IT", "13md_uyV_BNG": "Drifttekniker, IT",
                     "h324_DPT_7Tw": "Storhushållsföreståndare", "XGyi_i4B_ME7": "Köksmästare och souschefer",
                     "1CX5_mZw_Vcq": "Övriga utbildare och instruktörer",
                     "mcRJ_kq2_jFr": "Övriga pedagoger med teoretisk specialistkompetens",
                     "7n3y_FTE_qSe": "Piloter m.fl.", "yVZH_zHh_6fG": "Trafiklärare",
                     "H2Zj_eL9_x28": "Gruppledare för kontorspersonal", "dUuh_wwa_FGj": "Övriga förmedlare",
                     "fsnw_ZCu_v2U": "Arbetsförmedlare", "48KE_fDE_ryL": "Evenemangs- och reseproducenter m.fl.",
                     "AuTi_FEo_j3R": "Speditörer och transportmäklare", "8A1f_sxQ_adT": "Ordersamordnare m.fl.",
                     "xuSE_mtB_Mir": "Övriga yrken inom kultur och underhållning",
                     "PH2M_uV4_r72": "Inspicienter och scriptor m.fl.",
                     "CSUf_ZVM_a7Z": "Inredare, dekoratörer och scenografer m.fl.", "3J38_DNz_YKK": "Fotografer",
                     "FpPP_jqK_xzT": "Friskvårdskonsulenter och hälsopedagoger m.fl.",
                     "aA2Z_9XX_R1v": "Fritidsledare m.fl.", "EA6o_N1N_QtT": "Idrottstränare och instruktörer m.fl.",
                     "AwXA_EM4_71n": "Professionella idrottsutövare", "x5rn_vga_gRK": "Pastorer m.fl.",
                     "BWwk_fYX_S5B": "Behandlingsassistenter och socialpedagoger m.fl.", "rNcE_Zno_pD8": "Poliser",
                     "oEmP_NC1_sXE": "Övriga handläggare", "vCAb_uuo_ek5": "Chefssekreterare och VD-assistenter m.fl.",
                     "wCx8_rY3_XUT": "Brandingenjörer och byggnadsinspektörer m.fl.",
                     "thZP_oR7_WrY": "Ingenjörer och tekniker inom bygg och anläggning",
                     "nuXg_Wjc_JPd": "Säkerhetsinspektörer m.fl.", "ZQiZ_6Kd_D3a": "Socialförsäkringshandläggare",
                     "4W6S_1kA_nrv": "Skattehandläggare", "FKCE_Anf_zGH": "Tull- och kustbevakningstjänstemän",
                     "85cq_6uK_4cK": "Inköpare och upphandlare", "oXSW_fbY_XrY": "Företagssäljare",
                     "9dLz_BCK_oDA": "Försäkringssäljare och försäkringsrådgivare",
                     "Ru4W_zn2_LEz": "Skadereglerare och värderare", "q81z_3oD_1qu": "Redovisningsekonomer",
                     "w6ud_quG_dgh": "Arbetsledare inom bygg, anläggning och gruva",
                     "K8yg_U4C_gkY": "Ingenjörer och tekniker inom maskinteknik", "FfMN_Bw1_qYR": "Banktjänstemän",
                     "aQpg_E2T_cWq": "Mäklare inom finans", "8q1B_5AR_CEM": "Djursjukskötare m.fl.",
                     "bExu_8gk_azk": "Laboratorieingenjörer",
                     "GC7L_Yw7_Mfo": "Tekniker, bilddiagnostik och medicinteknisk utrustning",
                     "wNrt_Ysj_WuT": "Biomedicinska analytiker m.fl.", "XjcN_5LT_bWx": "Receptarier",
                     "39yB_xPQ_ozd": "Tandtekniker och ortopedingenjörer m.fl.", "eRkT_cTv_H9v": "Flygtekniker",
                     "yct8_Pun_DWd": "Flygledare", "Rsoy_sS6_6c4": "Maskinbefäl",
                     "dMVD_VYy_kgg": "Arbetsledare inom tillverkning", "mis4_feV_uex": "Övriga ingenjörer och tekniker",
                     "ketV_G9W_GeR": "GIS- och kartingenjörer",
                     "3jEy_yuT_aVo": "Ingenjörer och tekniker inom gruvteknik och metallurgi",
                     "v9nU_6Fw_4Qg": "Ingenjörer och tekniker inom kemi och kemiteknik",
                     "nDaB_vdy_eAy": "Ingenjörer och tekniker inom elektroteknik",
                     "33Nt_DSs_YYA": "Ingenjörer och tekniker inom industri, logistik och produktionsplanering",
                     "n6iX_f2z_XfE": "Övriga yrken inom socialt arbete", "5uP5_Ugw_aVE": "Biståndsbedömare m.fl.",
                     "dJXy_Rpq_a2u": "Kuratorer", "pok1_ipJ_yzD": "Socialsekreterare", "wzeq_No2_LF1": "Diakoner",
                     "2L2s_EU8_PzR": "Präster", "iBUL_s38_izZ": "Skådespelare",
                     "rYSG_3WJ_LmC": "Regissörer och producenter av film, teater m.m.",
                     "gmRr_7tt_eHj": "Koreografer och dansare", "AWJF_wqZ_va9": "Musiker, sångare och kompositörer",
                     "34Tn_BQQ_tfo": "Bildkonstnärer m.fl.", "PQDm_Z63_zst": "Översättare, tolkar och lingvister m.fl.",
                     "SgNH_hag_n9D": "Journalister m.fl.", "zfve_s6G_tpc": "Författare m.fl.",
                     "sXgV_QFg_vpb": "Arkeologer och specialister inom humaniora m.m.",
                     "wVfo_Njx_1rB": "Bibliotekarier och arkivarier", "Qbvf_6nd_Esn": "Museiintendenter m.fl.",
                     "br6a_VVS_V3y": "Övriga jurister", "JfYV_ZGg_6rm": "Förvaltnings- och organisationsjurister",
                     "DZVN_v5g_Fco": "Affärs- och företagsjurister", "6aak_8eh_iwT": "Åklagare",
                     "qsJJ_EYo_fzA": "Domare", "q8wL_kdi_WaW": "Advokater", "UxT1_tPF_Kbg": "Övriga IT-specialister",
                     "BAeH_eg8_T2d": "IT-säkerhetsspecialister", "cBBa_ngH_fCx": "Systemförvaltare m.fl.",
                     "D9SL_mtn_vGM": "Systemtestare och testledare",
                     "Q5DF_juj_8do": "Utvecklare inom spel och digitala media",
                     "DJh5_yyF_hEM": "Mjukvaru- och systemutvecklare m.fl.",
                     "UXKZ_3zZ_ipB": "Systemanalytiker och IT-arkitekter m.fl.",
                     "k1Nx_auG_sNh": "Informatörer, kommunikatörer och PR-specialister",
                     "WX67_Pfb_WMN": "Marknadsanalytiker och marknadsförare m.fl.",
                     "bjqk_F3A_5Hk": "Personal- och HR-specialister", "jJF3_qaQ_Zsh": "Studie- och yrkesvägledare",
                     "3FP7_4Eg_x8P": "Speciallärare och specialpedagoger m.fl.",
                     "vPP6_rsw_dck": "Planerare och utredare m.fl.",
                     "Qo7p_wjf_gtv": "Lednings- och organisationsutvecklare", "Y6yY_SuR_hVh": "Övriga ekonomer",
                     "WZCM_nfS_eAk": "Nationalekonomer och makroanalytiker m.fl.",
                     "Bbkh_nrm_2J8": "Traders och fondförvaltare",
                     "pTr9_RBT_9ur": "Finansanalytiker och investeringsrådgivare m.fl.", "Uw4n_UB2_RCW": "Controller",
                     "cRmd_536_pT1": "Revisorer m.fl.", "5ek3_Cgq_WeZ": "Förskollärare",
                     "CFW8_eBa_NaV": "Fritidspedagoger", "oQUQ_D11_HPx": "Grundskollärare",
                     "4KhP_FxL_uZ5": "Gymnasielärare", "2x2V_UeL_6ke": "Lärare i yrkesämnen",
                     "RPBE_x5M_cXX": "Övriga universitets- och högskolelärare", "NNK9_F1o_pK5": "Doktorander",
                     "P7eS_nP8_3dy": "Forskarassistenter m.fl.", "cYCo_PxY_zQd": "Universitets- och högskolelektorer",
                     "Gm5j_S2Y_aTB": "Professorer", "vTgW_pnr_QMp": "Övriga specialister inom hälso- och sjukvård",
                     "PUhV_DVQ_MM6": "Optiker", "pzch_NRY_9ZF": "Audionomer och logopeder", "Pq5C_SPC_bRA": "Dietister",
                     "TMsM_oNw_j6z": "Apotekare", "TPH4_2AM_isT": "Arbetsterapeuter",
                     "s7vU_FhY_L5Z": "Fysioterapeuter och sjukgymnaster",
                     "aWki_4uA_adn": "Kiropraktorer och naprapater m.fl.", "HPxT_fQJ_zwT": "Tandläkare",
                     "WL4E_r4b_ek4": "Veterinärer", "UUQi_UJ5_MSb": "Psykoterapeuter", "VYeq_GDF_a2g": "Psykologer",
                     "6zAR_EHM_Kwj": "Övriga specialistsjuksköterskor", "1tGC_fXC_mxX": "Röntgensjuksköterskor",
                     "6Fu3_qQA_9DK": "Företagssköterskor", "Bywn_Doc_3VF": "Skolsköterskor",
                     "NFUg_y9y_1TY": "Barnsjuksköterskor", "cuaN_Rj5_YCc": "Operationssjuksköterskor",
                     "dyWo_sDb_MLQ": "Intensivvårdssjuksköterskor", "6eAB_cbY_i9T": "Geriatriksjuksköterskor",
                     "PHJN_fva_yxs": "Ambulanssjuksköterskor m.fl.", "bEDv_SL7_VrL": "Psykiatrisjuksköterskor",
                     "oj4c_P4b_cja": "Distriktssköterskor", "nrVt_xUL_KdQ": "Anestesisjuksköterskor",
                     "7XXd_4St_nit": "Barnmorskor", "Z8ci_bBE_tmx": "Grundutbildade sjuksköterskor",
                     "gXqW_s5Q_ZjN": "Övriga läkare", "ek9W_qqp_XpF": "AT-läkare", "seu9_VoQ_VNo": "ST-läkare",
                     "nu7c_QuB_KVQ": "Specialistläkare",
                     "Uw64_cuc_U3H": "Arbetsmiljöingenjörer, yrkes- och miljöhygieniker",
                     "puim_6PY_DGj": "Miljö- och hälsoskyddsinspektörer",
                     "rE1E_m3d_cx1": "Specialister inom miljöskydd och miljöteknik",
                     "PF4H_kQD_zpL": "Övriga designer och formgivare",
                     "Mbt6_3ko_DiD": "Designer inom spel och digitala medier",
                     "8Bmh_vtM_zts": "Grafisk formgivare m.fl.", "DqLe_pH6_jyj": "Industridesigner",
                     "GMDo_DVo_Yzh": "Lantmätare", "Pd51_xnr_KCY": "Planeringsarkitekter m.fl.",
                     "ETAR_ggZ_Wuw": "Landskapsarkitekter", "41KB_bfk_5jR": "Arkitekter m.fl.",
                     "xm5G_vGn_iyk": "Civilingenjörsyrken inom logistik och produktionsplanering",
                     "XeBP_nMe_pXx": "Civilingenjörsyrken inom bygg och anläggning",
                     "SPYW_7Z1_ShT": "Civilingenjörsyrken inom elektroteknik",
                     "PRQn_9yw_NJA": "Civilingenjörsyrken inom maskinteknik",
                     "qn2U_dB9_Cmh": "Civilingenjörsyrken inom kemi och kemiteknik",
                     "rwpH_6RA_XTT": "Civilingenjörsyrken inom gruvteknik och metallurgi",
                     "mp2Y_vyC_RFV": "Övriga civilingenjörsyrken",
                     "3LdB_WEJ_8Ms": "Specialister och rådgivare inom skogsbruk",
                     "XurM_DVs_ggn": "Specialister och rådgivare inom lantbruk m.m.",
                     "dsK3_hkt_zGy": "Farmakologer och biomedicinare", "e9FK_VKh_ahW": "Växt- och djurbiologer",
                     "Z2MZ_eYc_Lfv": "Cell- och molekylärbiologer m.fl.", "jv1x_tpK_32p": "Statistiker",
                     "ZA1N_Z2B_nFQ": "Matematiker och aktuarier", "V7Yz_4WV_g4z": "Geologer och geofysiker m.fl.",
                     "eofS_oZs_RZi": "Kemister", "LP8B_auW_wMp": "Meteorologer",
                     "KM7d_Z6Y_LCJ": "Fysiker och astronomer", "NQTS_fou_E6u": "Övriga chefer inom övrig servicenäring",
                     "wby2_mwq_fWV": "Chefer inom friskvård, sport och fritid", "Fu4x_7KG_YgC": "Chefer inom handel",
                     "hZLv_S5f_jo2": "Restaurang- och kökschefer", "eX3C_MtJ_nPA": "Hotell- och konferenschefer",
                     "9Mn4_PNA_7Yz": "Chefer inom bank, finans och försäkring",
                     "sWzF_6pd_Y6L": "Övriga chefer inom samhällsservice",
                     "x9mo_1VH_Rps": "Chefer och ledare inom trossamfund", "coyZ_Cyv_Srw": "Chefer inom äldreomsorg",
                     "NxXz_fBh_w7j": "Chefer inom socialt och kurativt arbete",
                     "womV_SEj_CAH": "Chefer inom hälso- och sjukvård", "g6eC_Fwu_B3J": "Övriga chefer inom utbildning",
                     "z8di_AqX_GBr": "Chefer inom grund- och gymnasieskola samt vuxenutbildning",
                     "1uau_t1u_rDL": "Chefer inom förskoleverksamhet",
                     "9QMD_sZ3_ZjX": "Förvaltare inom skogsbruk och lantbruk m.fl.",
                     "pf1K_PTz_frm": "Produktionschefer inom tillverkning",
                     "7mn2_kA9_ftp": "Driftchefer inom bygg, anläggning och gruva",
                     "4GAs_rKs_2Ne": "Fastighets- och förvaltningschefer",
                     "8SPu_K1k_5qR": "Chefer inom arkitekt- och ingenjörsverksamhet",
                     "QG4f_MCh_vND": "Forsknings- och utvecklingschefer",
                     "nNxJ_YWy_vR2": "Inköps-, logistik- och transportchefer", "eWiB_mGP_MjE": "IT-chefer",
                     "uxst_tkg_zDh": "Övriga administrations- och servicechefer",
                     "Pz9M_srJ_Lc9": "Försäljnings- och marknadschefer",
                     "mpuA_s56_Hsy": "Informations-, kommunikations- och PR-chefer",
                     "X1Ac_Y9P_PJo": "Förvaltnings- och planeringschefer", "mNoS_AN5_drn": "Personal- och HR-chefer",
                     "mZsb_A1K_Gec": "Ekonomi- och finanschefer", "3i4a_Ufc_qpp": "Verkställande direktörer m.fl.",
                     "qVHD_Hj5_PXh": "Politiker", "ddGt_fEH_432": "General-, landstings- och kommundirektörer m.fl.",
                     "ajwx_FvL_YuY": "Chefstjänstemän i intresseorganisationer", "KiVA_anS_RJh": "Officerare",
                     "dhgR_9jk_95r": "Specialistofficerare", "jXUQ_rUV_iBv": "Soldater m.fl.",
                     "jY19_knH_MJp": "Undersköterskor, hemtjänst, hemsjukvård, äldreboende och habilitering",
                     "LP4Z_ABV_gRm": "Undersköterskor, vård- och specialavdelning och mottagning"}

occupation_fields = {"X82t_awd_Qyc": "Administration, ekonomi, juridik",
                     "j7Cq_ZJe_GkT": "Bygg och anläggning",
                     "apaJ_2ja_LuF": "Data/IT",
                     "Uuf1_GMh_Uvw": "Kropps- och skönhetsvård",
                     "RPTn_bxG_ExZ": "Försäljning, inköp, marknadsföring",
                     "PaxQ_o1G_wWH": "Hantverk",
                     "ScKy_FHB_7wT": "Hotell, restaurang, storhushåll",
                     "NYW6_mP6_vwf": "Hälso- och sjukvård",
                     "wTEr_CBC_bqh": "Industriell tillverkning",
                     "yhCP_AqT_tns": "Installation, drift, underhåll",
                     "9puE_nYg_crq": "Kultur, media, design",
                     "whao_Q6A_ScE": "Sanering och renhållning",
                     "VuuL_7CH_adj": "Naturbruk",
                     "kJeN_wmw_9wX": "Naturvetenskap",
                     "MVqp_eS8_kDZ": "Pedagogik",
                     "GazW_2TU_kJw": "Yrken med social inriktning",
                     "E7hm_BLq_fqZ": "Säkerhet och bevakning",
                     "6Hq3_tKo_V57": "Yrken med teknisk inriktning",
                     "ASGV_zcE_bWf": "Transport, distribution, lager",
                     "bh3H_Y3h_5eD": "Chefer och verksamhetsledare",
                     "bH5L_uXD_ZAX": "Militära yrken"}

employment_type = {
    "kpPX_CNN_gDU": "Tillsvidareanställning (inkl. eventuell provanställning)",
    "PFZr_Syz_cUq": "Vanlig anställning",
    "sTu5_NBQ_udq": "Tidsbegränsad anställning",
    "gro4_cWF_6D7": "Vikariat",
    "1paU_aCR_nGn": "Behovsanställning",
    "EBhX_Qm2_8eX": "Säsongsanställning",
    "Jh8f_q9J_pbJ": "Sommarjobb / feriejobb"

}
