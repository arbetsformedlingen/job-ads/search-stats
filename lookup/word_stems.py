words = [
    '#jobbjustnu',
    '2022',
    'abb',
    'abf',
    'academic',
    'adecco',
    'admin',
    'administrativt',
    'afry',
    'ahlsell',
    'alarm',
    'allegio',
    'allo',
    'alm',
    'ambass',
    'ambulan',
    'ams',
    'andr',
    'andraspråk',
    'anicur',
    'anticimex',
    'apoteket',
    'april',
    'arabic',
    'arbet',
    'arbetsmiljöverket',
    'ark',
    'arriv',
    'assistanc',
    'assistant',
    'ate',
    'attendo',
    'avarn',
    'avfall',
    'avlopp',
    'avonov',
    'axfood',
    'babysitting',
    'bad',
    'bauhaus',
    'beij',
    'bemanni',
    'bemanning',
    'bil',
    'bilbolaget',
    'biltem',
    'bim',
    'biträd',
    'blank',
    'blomsterbod',
    'blomsterl',
    'boend',
    'bosch',
    'bostad',
    'bravid',
    'bravur',
    'bt',
    'budbil',
    'burg',
    'buss',
    'bygg',
    'byggmax',
    'bär',
    'båt',
    'camp',
    'campus',
    'capio',
    'caverion',
    'cfd',
    'chain',
    'chatt',
    'chopchop',
    'city',
    'civilt',
    'clarion',
    'clas',
    'cleaning',
    'clockwork',
    'commerc',
    'compass',
    'consult',
    'consulting',
    'content',
    'coop',
    'coor',
    'coronavirus',
    'coronaviruset',
    'covid',
    'cramo',
    'csn',
    'cykel',
    'cytiv',
    'dag',
    'dagis',
    'dagtid',
    'dansktal',
    'deltid',
    'derom',
    'devic',
    'dhl',
    'didaktik',
    'digital',
    'distan',
    'dollarstor',
    'domstol',
    'donald',
    'drottning',
    'dygn',
    'dygnspass',
    'däck',

    'elev',
    'elgigant',
    'ellevio',
    'elon',
    'eltel',
    'embassy',
    'english',
    'enso',
    'eon',
    'epiroc',
    'erikshjälp',
    'essity',
    'eterni',
    'export',

    'fabrik',
    'familjecentral',
    'finan',
    'finsktal',
    'first',
    'fitness',
    'flykting',
    'folkhögskol',
    'folksam',
    'folkuniversitetet',
    'fonus',
    'food',
    'foodor',
    'fortifikationsverket',
    'fra',
    'french',
    'fritid',
    'fru',
    'fut',
    'förskoleklass',
    'försv',
    'försäkringskassan',
    'försörjningsstöd',
    'gekås',
    'geosecm',
    'german',
    'gil',
    'glimakr',
    'goteb',
    'grafisk',
    'granngård',
    'gross',
    'group',
    'grön',
    'guldfynd',
    'gym',
    'göing',
    'götaland',
    'hamn',
    'hand',
    'helg',
    'helgjobb',
    'helgskift',
    'heltidsment',
    'hem',
    'hemfrid',
    'hemifrån',
    'hemköp',
    'hemtex',
    'hkk',
    'hms',
    'hornbach',
    'hr',
    'hsb',
    'human',
    'hund',
    'hälso',
    'häst',
    'ica',
    'ies',
    'ike',
    'iket',
    'indisk',
    'inpeopl',
    'instabox',
    'interim',
    'internationell',
    'intersport',
    'ivo',
    'job',
    'jollyroom',
    'jordbruksverket',
    'jul',
    'juni',
    'jysk',
    'kappahl',
    'kick',
    'king',
    'klarn',
    'klim',
    'kma',
    'kok',
    'kollo',
    'komatsu',
    'kommun',
    'komvux',
    'konfer',
    'konsult',
    'kont',
    'kontaktperson',
    'korset',
    'kraft',
    'krav',
    'kulturarv',
    'kundcent',
    'kvalitet',
    'kväll',
    'kyrk',
    'kyrkan',
    'köpenhamn',
    'lagerhaus',
    'lantmän',
    'lantmäteriet',
    'lastbil',
    'led',
    'lerni',
    'leveran',
    'lidl',
    'lido',
    'lif',
    'lifec',
    'lindex',
    'ljud',
    'lkab',
    'logent',
    'länsförsäkring',
    'länsstyr',
    'länstyr',
    'lär',
    'lärling',
    'lön',
    'manpow',
    'mar',
    'marbell',
    'markis',
    'markn',
    'match',
    'mathem',
    'max',
    'maxi',
    'mc',
    'mcdonald',
    'medarbet',
    'mediamark',
    'medical',
    'medicinsk',
    'mekonom',
    'migrationsverket',
    'mio',
    'mittuniversitetet',
    'montico',
    'msb',
    'mtr',
    'muse',
    'musei',
    'museum',
    'mynd',
    'myr',
    'måltid',
    'måltidbiträd',
    'nattjobb',
    'natur',
    'ncc',
    'net',
    'nkt',
    'nobin',
    'norde',
    'nordic',
    'norlandi',
    'norrmejeri',
    'northvolt',
    'nwt',
    'nya',
    'nyanländ',
    'nystartsjobb',
    'nystart',
    'nytid',
    'ock',
     'okq8',
    'omgåend',
    'området',
    'ord',
    'oslo',
    'padel',
    'park',
    'peab',
    'pelican',
    'pensionsmynd',
    'person',
    'personal',
    'planering',
    'plantag',
    'platsbank',
    'polismynd',
    'pooli',
    'post',
    'postnord',
    'praktik',
    'pressbyrån',
    'previ',
    'produktion',
    'projek',
    'ptp',
    'quality',
    'randst',
    'ranst',
    'regeringskansliet',
    'region',
    'remot',
    'res',
    'resur',
    'riksbygg',
    'ris',
    'ritual',
    'russian',
    'röd',
    'saab',
    'safero',
    'sahlgrensk',
    'samhall',
    'samtal',
    'sandvik',
    'sca',
    'scandic',
    'scani',
    'schenk',
    'scienc',
    'seb',
    'second',
    'sect',
    'securit',
    'seng',
    'server',
    'servic',
    'servicecent',
    'sesol',
    'sexuell',
    'siem',
    'sis',
    'sjukpenning',
    'skans',
    'skansk',
    'skaraborg',
    'skatteverket',
    'skog',
    'skolan',
    'skolinspektion',
    'skolverket',
    'snar',
    'socialt',
    'sodexo',
    'sommarjob',
    'sommarskol',
    'sommarvikari',
    'sos',
    'spanish',
    'speaking',
    'spsm',
    'ssab',
    'st',
    'stad',
    'stadium',
    'stadsmission',
    'start',
    'stat',
    'stor',
    'strong',
    'student',
    'studi',
    'studieförbund',
    'städ',
    'subway',
    'summ',
    'supply',
    'support',
    'sva',
    'sweco',
    'swedavi',
    'swedbank',
    'synsam',
    'systembolaget',
    'särskild',
    'södr',
    'sök',
    'teach',
    'tele2',
    'telen',
    'teli',
    'tillträd',
    'tillväxtverket',
    'timanställning',
    'timvikari',
    'toyot',
    'trafik',
    'trafikskol',
    'trafikverket',
    'transportstyr',
    'traum',
    'tryckeri',
    'tull',
    'tullverket',
    'underhåll',
    'undervisningsgrupp',
    'ungdom',
    'uniflex',
    'universitet',
    'ups',
    'utbildning',
    'utveckling',
    'vaccin',
    'valmet',
    'vapiano',
    'vattenfall',
    'verksam',
    'vikari',
    'vinnergi',
    'vism',
    'volvo',
    'västr',
    'våld',
    'webhelp',
    'wellness',
    'westinghous',
    'willy',
    'work',
    'writ',
    'xxl',
    'xylem',
    'zoo',
    'åhl',
    'åklagarmynd',
    'årskur',
    'öpp',
    'öppn',
    'östr',
    'övergrepp',
    'advokatbyrå',
    'affär'
    'akademibokhandeln',
    'akademin',
    'akademisk',
    'aktivitet',
    'akut',
    'arbetsträning',
    'biblioteket',
]
