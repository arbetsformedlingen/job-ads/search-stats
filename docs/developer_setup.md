# Set up Search-Trends as a developer

### Logins required
This code is dependent on how Arbetsförmedlingen JobTech has configured logging for JobSearch.
You will not be able to run it unless you have access to the OpenSearch instance where these logs are stored.
Accessing the log database is only possible when Arbetsförmedlingens VPN is used.

## Python dependencies

This project uses [Poetry](https://python-poetry.org/) to manage dependencies.

### Install poetry

`python -m pip install poetry`

### `Install dependencies

`poetry install`

### Environment variables
```
OPENSEARCH_HOST: The OpenSearch instance where logs are stored, use plain url/ip address without http* prefix
OPENSEARCH_PORT : The port used by the OpenSearch instance, defaults to 9200
OPENSEARCH_INDEX_PREFIX: Index pattern for logs, e.g "filebeat-*"  
AWS_BUCKET: The url to the AWS bucket where public files should be uploaded. No http* prefix
MINIO_BUCKET: The Minio bucket used for storing non-public files
LOGS_FROM_HOST: If you want to use anything else than prod-JobSearch, put it here. No http* prefix
```

## Secrets
Configured in OpenShift

## Build Docker/Podman image

`podman build -t search-stats:dev .`

Create a file `env` containing necessary environment variables:

```
OPENSEARCH_HOST=my_opensearch_host
OPENSEARCH_PORT=9200
OPENSEARCH_USER=myuser
OPENSEARCH_PWD=mypassword
OPENSEARCH_INDEX_PREFIX=filebeat-*
WORK_DIR=/tmp
```

Run with: `podman run --env-file=env search-stats:dev collect_stats`

## Running it locally
Set variables as above in your terminal or the IDE's run configuration. You must have access to the OpenSearch instance where the logs are stored.
Run `python main.py` this will collect logs from yesterday, save temp files, summarize them and create output files.

There is also a command-line script defined in pyproject.toml:
```yaml
[tool.poetry.scripts]
collect_stats = 'search_trends.main:main'
```

If you want it to complete quicker, use the OpenSearch instance that collects logs from testing environments, set `LOGS_FROM_HOST=staging-jobsearch-api.jobtechdev.se`, there is not that much traffic to that environment (and not as much logs)

## Deployment
Configuration is stored in the [search-trends-infra repository](https://gitlab.com/arbetsformedlingen/job-ads/search-trends-infra)
To be able to access the OpenSearch instances where the logs are, search-trends must run in one of the onprem Openshift clusters.
