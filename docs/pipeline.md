# Tekton Pipeline

## Infra Repo:
https://gitlab.com/arbetsformedlingen/job-ads/search-trends-infra/-/blob/main/kustomize/base/pipeline.yaml?ref_type=heads

## OpenShift
[onprem-test](https://console-openshift-console.apps.joct4.arbetsformedlingen.se/add/ns/search-trends-develop)
[onprem-prod](https://console-openshift-console.apps.jocp4.arbetsformedlingen.se/k8s/cluster/projects/search-trends-prod)



## Configuration 
- S3 bucket for final upload
- Minio bucket for temporary storage, and storage of non-public files
- Konfiguration for shared workspace
- Access to Log-OpenSearch
- Proxy for accessing AWS (or anything else outside of on-prem)

## Params
From the Tekton pipeline
```yaml
  params:
  - name: date
    description: Date to extract # Typically yesterday
    default: ""
  - name: public-bucket
    description: Destination bucket for processed files on S3  # upload to data.jobtechdev.se
    default: data.jobtechdev.se-adhoc-test
  - name: private-bucket
    description: Destination bucket for private processed files on Minio  # temp files and non-public files
    default: search-trends-dev
```

## Environment variables
From the config map for search-trends-dev
```yaml
data:
  OPENSEARCH_HOST: 10.126.2.108
  OPENSEARCH_PORT : "9200"
  OPENSEARCH_INDEX_PREFIX: filebeat-*
  AWS_BUCKET: data.jobtechdev.se-adhoc-test
  MINIO_BUCKET: search-trends-dev
  LOGS_FROM_HOST: staging-jobsearch-api.jobtechdev.se
```


## Secrets
Saved as Secrets under Workloads right now. Should be moved to VaultWarden.
- `elastic`: login to Elastic/OpenSearch. Should be renamed since we only use OpenSearch now.
- `file-publish-s3`: variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
- `minio-secret` variable MC_HOST_MINIO which is a connection string
An [issue](https://gitlab.com/arbetsformedlingen/job-ads/search-trends/-/issues/55) is created for renaming of variables.
