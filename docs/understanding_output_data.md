Given the shortened file `example.json` in this folder, let´s take a look at some of the content

`"q_approved": [["stockholms län", 26769], ["personlig assistent", 25706]]`
  
`q_approved` are free text searches that have passed through a filter to remove personally identifiable information.
In this case, 26769 searches were done for "stockholms län" in a day.
The list of key-value pairs are normally longer, in this file only a few are included.
Free text searches are converted to lower case-

``` 
region": [["CifL_Rzy_Mku", 160613], ["CaRE_1nn_cSU", 77012]], 
"region-label": [["Stockholms län", 160613], ["Skåne län", 77012]],
```
This search using the `region` paramater with a Taxonomy concept id.
The data is repeated with the concept id's human-readable label used instead. 

The same pattern is used for other searches that uses codes (Taxonomy concept ids or SSYK codes). 
First the parameter and concept id, then a separate entry with the concept id's label to make it easier to read.
You can read more about Taxonomy concept ids here: https://atlas.jobtechdev.se/page/taxonomy.html

```
"municipality": [["AvNB_uwa_6n6", 107983], ["PVZL_BQT_XtL", 87873]], 
"municipality-label": [["Stockholm", 107983], ["Göteborg", 87873]],
"country": [["-199", 799], ["-i46j_HmG_v64", 458]], 
"country-label": [["-Sverige (old)", 799], ["-Sverige", 458]],
"occupation-name": [["pBhS_6fg_727", 1192], ["sizv_uPq_nWf", 1185]],
"occupation-name-label": [["Butikssäljare, dagligvaror/Medarbetare, dagligvaror", 1192], ["Lagerarbetare", 1185]],
"occupation-group": [["oQUQ_D11_HPx", 23658], ["YKL2_FCB_1yr", 23229]],
"occupation-field-label": [["Administration, ekonomi, juridik", 39892], ["Chefer och verksamhetsledare", 24073]],
"occupation-collection": [["UdVa_jRr_9DE", 50275], ["-UdVa_jRr_9DE", 7304]],
"occupation-collection-label": [["Yrkessamling, yrken utan krav på utbildning", 50275], ["-Yrkessamling, yrken utan krav på utbildning", 7304]],  # -means exclude
```


Boolean parameters
`"driving-license-required": [["false", 41678]],`
Excluding ads where driving license is required is the most common use of this parameter.
It could be "true" values as well, but it's generally not how this parameter is used.

The other fields in the example file or daily output file follows a similar pattern:
`"field": [ ["value1", frequency], ["value2", frequency] ]`
All fields with descriptions are found in the file `fields.md`
