# Change log

## 1.1.3 (August 2024)

* Fix bug that accidentally removed occupation-group-label and occupation-field from public files.

## 1.1.2 (January 2023)

* Update Python version in Dockerfile

## 1.1.1 (October 2022)

* Updated the handle of certifictes

## 1.1.0 (October 2022)

* Changed database from Elasticsearch to OpenSearch

## 1.0.1 (September 2022)

* Update Python in Dockerfile to 3.10.7

## 1.0.0 (June 2022)

* First version
