FROM docker.io/library/python:3.10.15-slim-bookworm

COPY . /app
WORKDIR /app

RUN mkdir /app/out && \
    python -m pip install --upgrade build &&\
    python -m build --outdir /app/out


FROM docker.io/library/ubuntu:22.04

WORKDIR /app
COPY --from=0 /app/out/search_trends-*-py3-none-any.whl ./
RUN apt-get -y update &&\
    apt-get -y install python3 python3-pip &&\
    python3 -m pip install search_trends-*-py3-none-any.whl
