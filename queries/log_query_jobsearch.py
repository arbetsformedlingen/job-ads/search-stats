def query(after: str, before: str, host: str) -> dict:
    return {
        "fields": [{"field": "calamari.request.query_string"}],  # this field is where the data we are interested in is stored
        "query": {
            "bool": {
                "filter": [
                    {
                        "range": {
                            "@timestamp": {
                                "format": "strict_date_optional_time",
                                "gte": after,  # typically one hour interval here
                                "lte": before,
                            }}},
                    {"exists": {"field": "calamari.request.query_string"}},
                    {"match_phrase": {"calamari.request.path": "/search"}}, # we are only interested in this endpoint, not the other endpoints in JobSearch
                    {"match_phrase": {"calamari.host": host}}
                ],
                "must_not": [  # exclude liveness queries, queries to get number of ads
                    {"match_phrase": {"url.query": "?region=01&limit=0"}},
                    {"match_phrase": {"url.query": '?offset=0&limit=0'}},
                    {"match_phrase": {"url.query": '?offset=0&limit=0&show-count=false'}}
                ]
            }
        }
    }
