from loguru import logger
from datetime import datetime, timedelta
from search_trends import settings
from search_trends.file_handler import write_raw_logs
from search_trends.opensearch import scan
from search_trends.query_string_parser import parse_scan_result
from queries.log_query_jobsearch import query as log_query


def days_ago(days_in_the_past: int = 1) -> str:
    from_day = datetime.now() - timedelta(days=days_in_the_past)
    from_day_query = from_day.strftime(settings.DATE_FORMAT_QUERY)
    return from_day_query


def create_temp_files_from_whole_day(working_date: str) -> None:
    start = datetime.now()
    daily_total = 0
    for hour in range(0, 24):
        after = f"{working_date}T{hour:02d}:00:00.000Z"
        before = f"{working_date}T{hour:02d}:59:59.999Z"
        logger.info(f"Time interval: {after} - {before}")
        query = log_query(after, before, settings.LOGS_FROM_HOST)
        scan_results = scan(query)
        raw_results = parse_scan_result(scan_results)
        file_name = f"{working_date}T{hour:02d}Z-RAW.txt"
        write_raw_logs(raw_results, file_name=file_name, day=working_date)
        hourly_total = len(raw_results)
        daily_total += hourly_total
        logger.debug(f"{hourly_total} rows in time interval {after} - {before} written to {file_name}")

    logger.info(f"Completed downloading {daily_total} log rows from {working_date} in {datetime.now() - start}")

