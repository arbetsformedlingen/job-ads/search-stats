import urllib.parse

from loguru import logger

from search_trends import settings


def decode_url_formatting(word: str) -> str:
    return urllib.parse.unquote(word)


def clean_word(word: str) -> str:
    try:
        word = decode_url_formatting(word)
    except AttributeError as e:
        logger.error(f"{word} - {e}")
        return ""
    word = word.strip()
    word = word.lower()

    # common encoding problems
    if "ã¥" in word:
        word = word.replace("ã¥", "å")
    if "ã¤" in word:
        word = word.replace("ã¤", "ä")
    if "ã¶" in word or "ã–" in word:
        for c in ["ã¶", "ã–"]:
            word = word.replace(c, "ö")
    # alphanumeric but unwanted
    if "¹" in word or "𝕜" in word:
        word = word.replace("¹", " ").replace("𝕜", " ")

    if word.isalnum():  # alphanumeric, most common
        return word
    elif is_percent(word):  # e.g. 75%
        return word
    else:
        # remove characters that are not alphanumeric
        new_word = ""
        for char in word:
            if char.isalnum():
                new_word += char
            else:
                new_word += " "
    return new_word.strip()


def is_percent(word: str) -> bool:
    # OK: 100%, 75%
    # Not OK: 5%, 25 %, 1000%, xy%
    return word.endswith("%") and 3 <= len(word) <= 4 and word[:-1].isdigit()


def format_public(complete_result_dict: dict) -> dict:
    """
    create new dictionary with only fields we want to release
    """
    public_result = {}
    for key in settings.PUBLIC_FIELDS:
        if value := complete_result_dict.get(key, None):
            public_result[key] = value
    return public_result
