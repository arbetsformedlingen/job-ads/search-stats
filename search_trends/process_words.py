from nltk.stem import SnowballStemmer
from loguru import logger

from search_trends import settings as settings
from lookup import whitelist_creator, employer_field
from search_trends.cleaner import clean_word, is_percent

whitelist = whitelist_creator.create_whitelist()
whitelisted_employers = set(employer_field.employers)


def is_word_approved(cleaned_word: str) -> bool:
    if cleaned_word in whitelist:
        return True
    else:
        return complex_search(cleaned_word)


def check_school(word: str) -> bool:
    temp_word = word
    # truncate to "skola"
    if word.endswith('skolans'):
        temp_word = word[:-2]
    elif word.endswith("skolan"):
        temp_word = word[:-1]
    return temp_word in whitelist


def complex_search(word: str) -> bool:
    words = word.split()
    successes = set()
    not_found = []
    found_words = []
    for w in words:
        if len(w) < settings.MINIMUM_WORD_LENGTH:
            continue  # short words in a sentence are generally ok
        if w in whitelist:
            successes.add(True)
            found_words.append(w)
        elif search_word_stems(w):
            successes.add(True)
            found_words.append(w)
        elif '%' in w and is_percent(w):
            successes.add(True)
            found_words.append(w)
        elif 'skola' in w:
            if school_ok := check_school(w):
                successes.add(school_ok)
                found_words.append(w)

        else:
            not_found.append(w)
            successes.add(False)

    if len(successes) == 1 and True in successes:
        return True
    # logger.debug(f"OK: {found_words} - Not found: {not_found} - Full query: {word}")
    return False


def search_word_stems(word: str) -> bool:
    stemmer = SnowballStemmer("swedish")
    stemmed = stemmer.stem(word)
    result = stemmed in whitelist
    return result


def find_matches(list_of_words: list) -> tuple:  # returns a tuple of lists
    logger.info(f"Checking {len(list_of_words)} items")
    unapproved = []
    approved = []
    not_checked = []
    for word in list_of_words:

        if len(word) > settings.MINIMUM_COUNT and len(word) >= settings.MINIMUM_WORD_LENGTH:
            cleaned_word = clean_word(word)
            if is_word_approved(cleaned_word):
                approved.append(cleaned_word)
            else:
                unapproved.append(cleaned_word)
        else:
            not_checked.append(word)

    logger.info(f"Approved: {len(approved)}")
    logger.info(f"Unapproved: {len(unapproved)}")
    return approved, unapproved, not_checked


def is_employer(word):
    if is_organization_number(word):
        return True
    elif word in whitelisted_employers:
        return True
    elif word == '202100':  # prefix for public organizations
        return True
    elif is_word_approved(word):
        return True

    return False


def is_organization_number(number: str) -> bool:
    try:
        if len(number) < 10:  # organization numbers are 10 digits
            return False
        elif any([
            not number,
            not number.isdigit(),
            int(number[0]) == 0  # first digit is 0: it's probably a phone number
        ]):
            return False

        # check character 3-4, they must be >= 20 to be an organization number (i.e. no month)
        org_nr_part = number[2:4]
        if all([int(org_nr_part) < 20, org_nr_part.isdigit()]):
            return False
        else:
            return all([number.isdigit(), len(number) == 10])
    except Exception as e:
        logger.error(e)
        return False
