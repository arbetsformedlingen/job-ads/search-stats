from dateutil.parser import parse
from loguru import logger

from search_trends import settings
from search_trends.create_temp_files import create_temp_files_from_whole_day, days_ago
from search_trends.create_daily_file import daily_file


def is_date(date: str) -> bool:
    try:
        parse(date, fuzzy=False)
    except (ValueError, TypeError):
        return False

    return True


@logger.catch()
def main():
    if not is_date(settings.DATE):
        logger.info("No date set, using yesterday")
        working_date = days_ago()
    else:
        working_date = settings.DATE
    logger.info(f"start processing data from {working_date}")
    logger.info(
        f"Lower hits limit: {settings.MINIMUM_COUNT}, Lower word length limit: {settings.MINIMUM_WORD_LENGTH}"
    )
    logger.info(f"Collection logs from : {settings.LOGS_FROM_HOST}")
    create_temp_files_from_whole_day(working_date)
    daily_file(working_date)
