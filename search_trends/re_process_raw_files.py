
from create_daily_file import daily_file
from create_temp_files import days_ago

if __name__ == '__main__':
    """
    If you want to re-process the downloaded raw text files
    e.g. new whitelists, parameters added etc
    """

    for day in range(2, 13):
        days_back = days_ago(day)
        daily_file(days_back)
