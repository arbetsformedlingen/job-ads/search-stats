import glob
import json
from search_trends import settings
import os
from collections import Counter

from loguru import logger

if __name__ == '__main__':

    dirs = os.listdir(settings.OUTPUT_DIR)

    main_counter = Counter()
    days = []

    for directory in dirs:
        path = f'{settings.OUTPUT_DIR}/{directory}/*-unapproved-*'
        files = glob.glob(path)
        for f in files:
            logger.info(f"Reading from {f}")
            with open(f, encoding='utf8') as in_file:
                data = json.load(in_file)['q']
                main_counter.update(dict(data))
                days.append(directory)

    logger.info(f"Summary from {days}")
    for item in main_counter.most_common(100):
        logger.info(item)
    logger.info("finished")
