from loguru import logger

from lookup import taxonomy_occupation_names, taxonomy_occupation_groups_fields_collections, taxonomy_geo, taxonomy_languages


def translate_list(list_of_results: list, taxonomy_pairs: dict) -> list:
    translated_list = []
    for item in list_of_results:
        minus = False
        concept_id = item[0]
        if concept_id.startswith('-'):
            minus = True
        number = item[1]
        if minus:
            concept_id = concept_id[1:]
        if label := taxonomy_pairs.get(concept_id, None):
            if minus:
                label = f"-{label}"
            translated_list.append((label, number))
        else:
            if minus:
                concept_id = f"-{concept_id}"
            translated_list.append((concept_id, number))
    return translated_list


def add_translations(summarized_dict: dict) -> dict:
    for field, pairs in [

        ('municipality', taxonomy_geo.municipalities),
        ('region', taxonomy_geo.regions),
        ('country', taxonomy_geo.countries),

        ('occupation-name', taxonomy_occupation_names.occupation_names),
        ('occupation-group', taxonomy_occupation_groups_fields_collections.occupation_groups),
        ('occupation-field', taxonomy_occupation_groups_fields_collections.occupation_fields),
        ('occupation-collection', taxonomy_occupation_groups_fields_collections.occupation_collections),
        ('employment-type', taxonomy_occupation_groups_fields_collections.employment_type),
        ('worktime-extent', {"6YE1_gAC_R2G": "Heltid", "947z_JGS_Uk2": "Deltid"}),
        ('language', taxonomy_languages.languages),

        # not supported param
        ('lanid', taxonomy_geo.regions)
    ]:
        logger.info(f"Translating {field}")
        if temp := summarized_dict.get(field, None):
            translated = translate_list(temp, pairs)
            new_field_name = f"{field}-label"
            summarized_dict[new_field_name] = translated
        else:
            logger.error(f"No data for field {field}")
    return summarized_dict
