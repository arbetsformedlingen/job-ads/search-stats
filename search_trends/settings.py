import os
import pathlib

SERVICE_NAME = os.environ.get("SERVICE_NAME", default="jobsearch")
MINIMUM_COUNT = 0
MINIMUM_WORD_LENGTH = 2
RAW_FILE_PATTERN = "*-RAW.txt"
DATE_FORMAT_QUERY = "%Y-%m-%d"
DATE_FORMAT_INDEX = "%Y.%m.%d"

WORK_DIR = os.getenv("WORK_DIR", ".")
TMP_DIR = pathlib.Path(WORK_DIR + "/tmp")
OUTPUT_DIR = pathlib.Path(WORK_DIR)
DATE = os.getenv("DATE")

TOP_LEVEL = pathlib.Path(__file__).parent.resolve()
CACHE_DIR = "../lookup"
INTERNAL = "INTERNAL"
PUBLIC = "public"
LOGS_FROM_HOST = os.getenv("LOGS_FROM_HOST", "jobsearch.api.jobtechdev.se")

WHITELIST_FILE = "synonyms.p"
PUBLIC_SUFFIX = "-public.json"

OPENSEARCH_HOST = os.getenv("OPENSEARCH_HOST")
OPENSEARCH_PORT = os.getenv("OPENSEARCH_PORT", 9200)
OPENSEARCH_USER = os.getenv("OPENSEARCH_USER")
OPENSEARCH_PWD = os.getenv("OPENSEARCH_PWD")
OPENSEARCH_INDEX_PREFIX = os.getenv("OPENSEARCH_INDEX_PREFIX")
OPENSEARCH_TIMEOUT = 300
OPENSEARCH_SIZE = 10000
OPENSEARCH_CAFILE = "cert/af_bundle.pem"

# minio settings
MINIO_HOST = os.getenv("MINIO_HOST")
MINIO_USER = os.getenv("MINIO_USER")
MINIO_PWD = os.getenv("MINIO_PWD")

PUBLIC_FIELDS = [
    "q_approved",
    "region",
    "region-label",
    "municipality",
    "municipality-label",
    "country",
    "country-label",
    "occupation-name",
    "occupation-name-label",
    "occupation-group",
    "occupation-group-label",
    "occupation-field",
    "occupation-field-label",
    "occupation-collection",
    "occupation-collection-label",
    "driving-license-required",
    "type",
    "remote",
    "unspecified-sweden-workplace",
    "open_for_all",
    "worktime-extent",
    "employment-type",
    "experience",
    "stats",
    "abroad",
    "trainee",
    "qfields",
]
