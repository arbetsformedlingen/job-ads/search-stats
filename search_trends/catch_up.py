from .create_temp_files import days_ago, create_temp_files_from_whole_day


def get_last_week():
    for day in range(1, 8):
        log_date = days_ago(day)
        create_temp_files_from_whole_day(log_date)


if __name__ == "__main__":
    get_last_week()
