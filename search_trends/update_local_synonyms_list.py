"""
Script to preload synonyms from jobad enrichment API.
Saves a compiled file of synonyms to disc to speed up
start of jobsearch API.
"""

import time, logging, asyncio, requests, pickle
from enum import Enum

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)
logger = logging.getLogger(__name__)

ONTOLOGY_API_SYNONYMS_ENDPOINT = 'https://jobad-enrichments-api.jobtechdev.se/synonymdictionary'
FILE_NAME = f"../cache/synonyms.p"

"""
Enumeration class for the
synonym types.
"""


class SynonymType(Enum):
    COMPETENCE = 'COMPETENCE'
    TRAIT = 'TRAIT'
    GEO = 'GEO'
    SKILL = 'OCCUPATION'


"""
Fetch synonyms and return list iteration
"""


def main():
    logger.info(f"Fetching synonyms from {ONTOLOGY_API_SYNONYMS_ENDPOINT}")
    start_time = time.perf_counter()
    synonyms = asyncio.run(get_synonyms())

    try:
        # Synonyms is a list of four lists. Flatten this to one list.
        flattened_list = [item for sublist in synonyms for item in sublist]
        only_words = make_list_with_terms(flattened_list)
        save_to_file(only_words)
        end_time = time.perf_counter()
        calculated_time = str(round(end_time - start_time, 2))
        logger.info(f"Successfully loaded and saved synonyms to file. Finished in total of {calculated_time} seconds.")
    except TypeError as te:
        logger.critical("FAILED - One or more synonyms list was not loaded. Could not finish script.")
        raise

def make_list_with_terms(synonym_list: list) -> list:
    words = []
    for s in synonym_list:
        words.append(s['term'])
    return words



def save_to_file(synonyms: list):
    with open(FILE_NAME, 'wb') as f:
        pickle.dump(synonyms, f)

    logger.info(f"Saved synonyms to file {FILE_NAME}")


"""
Load synonyms from the four different
types and return a compiled list.
"""


async def get_synonyms() -> list:
    # loop = get_or_create_eventloop()
    result = await asyncio.gather(
        call_api_synonyms_endpoint(SynonymType.GEO.value),
        call_api_synonyms_endpoint(SynonymType.TRAIT.value),
        call_api_synonyms_endpoint(SynonymType.SKILL.value),
        call_api_synonyms_endpoint(SynonymType.COMPETENCE.value),
    )
    return result


"""
Generic function for calling the synonym endpoint
and returning the items in the api response.
Runs async.
"""


async def call_api_synonyms_endpoint(synonym_type: str) -> list:
    try:
        logger.info(f"Fetching {synonym_type}")
        start_time = time.perf_counter()
        loop = get_or_create_eventloop()
        payload = {'type': synonym_type, 'spelling': 'BOTH'}
        res = await loop.run_in_executor(None, requests.get, ONTOLOGY_API_SYNONYMS_ENDPOINT, payload)
        end_time = time.perf_counter()
        calculated_time = str(round(end_time - start_time, 2))
        logger.info(f"Fetched {synonym_type} in {calculated_time} seconds.")
        return res.json()['items']
    except Exception as ce:
        logger.critical("Failed to fetch synonyms from API.")


"""
To handle runtime error when no
event loop is found for asyncio.
"""


def get_or_create_eventloop() -> asyncio.BaseEventLoop:
    try:
        return asyncio.get_event_loop()
    except RuntimeError as ex:
        if "There is no current event loop in thread" in str(ex):
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            return asyncio.get_event_loop()


if __name__ == '__main__':
    main()
