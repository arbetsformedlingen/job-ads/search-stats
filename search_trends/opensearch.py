from opensearchpy import OpenSearch
import opensearchpy.helpers

from loguru import logger
from search_trends import settings


def create_opensearch_client() -> OpenSearch:
    opensearch_client = OpenSearch([{'host': settings.OPENSEARCH_HOST, 'port': settings.OPENSEARCH_PORT}],
                                   ca_certs=settings.OPENSEARCH_CAFILE,
                                   use_ssl=True,
                                   ssl_assert_hostname=False,
                                   verify_certs=False,
                                   ssl_show_warn=False,
                                   http_auth=(settings.OPENSEARCH_USER, settings.OPENSEARCH_PWD),
                                   timeout=settings.OPENSEARCH_TIMEOUT)
    info = opensearch_client.info()  # check connection
    logger.info(info)
    return opensearch_client


def scan(query: dict):
    logger.info("starting scan")
    client = create_opensearch_client()
    scan_result = opensearchpy.helpers.scan(client, index=settings.OPENSEARCH_INDEX_PREFIX, query=query, size=10000,
                                            scroll="15m")
    logger.info("scan finished, processing result")
    return scan_result
