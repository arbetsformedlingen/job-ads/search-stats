from loguru import logger

from search_trends import process_words


def parse_scan_result(scan_result) -> list:
    results = []
    for row in scan_result:
        if query := row.get("fields", {}).get("calamari.request.query_string", None):
            results.append(query[0])
    return results


def build_dict_from_raw_query_strings(raw_query_strings: list) -> dict:
    logger.info(f"Build param dict from {len(raw_query_strings)} log rows")
    param_dict = {}
    for row in raw_query_strings:
        qs_parts = row.split("&")
        for param in qs_parts:
            if param.rstrip():
                try:
                    key_val = param.split("=")
                    key = key_val[0].strip()
                    value = key_val[1].strip()
                    if values := param_dict.get(key, None):
                        # update existing list
                        values.append(value)
                        param_dict[key] = values
                    else:
                        # create new entry:
                        logger.debug(f"Added key {key}")
                        param_dict[key] = [value]
                except IndexError:
                    # logger.warning(f"IndexError for param: {param} in query string {row}")
                    pass
    logger.info("finished")
    return param_dict


def check_data(param_dict: dict) -> dict:
    q_queries = param_dict["q"]
    approved, unapproved, not_checked = process_words.find_matches(q_queries)
    param_dict["q_approved"] = approved
    param_dict["q_internal_unapproved"] = unapproved
    param_dict["q_internal_not_checked"] = not_checked
    param_dict.pop("q")
    if param_dict.get("employer", None):
        param_dict["employer-approved"] = check_employer(param_dict["employer"])

    return param_dict


def check_employer(list_of_employers: list) -> list:
    checked_employers = []
    not_ok = set()
    for employer in list_of_employers:
        if employer:
            employer = process_words.clean_word(employer)

            if process_words.is_employer(employer):
                checked_employers.append(employer)
            else:
                not_ok.add(employer)
                logger.debug(f"Employer {employer} not approved")

    return checked_employers
