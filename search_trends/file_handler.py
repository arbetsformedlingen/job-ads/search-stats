import json
from pathlib import Path
import pickle

from loguru import logger

from search_trends import settings

temp_file_path = settings.TMP_DIR
cache_file_path = settings.TOP_LEVEL / settings.CACHE_DIR


def write_json_output_file(
    data: dict, suffix: str, working_date: str, visibility: str = settings.INTERNAL
) -> str:
    file_name = f"{settings.SERVICE_NAME}-{working_date}-{suffix}-{visibility}.json"
    file_path = settings.OUTPUT_DIR / file_name

    file_path.parent.mkdir(exist_ok=True, parents=True)
    with open(file_path, "w", encoding="utf8") as f:
        json.dump(data, f, ensure_ascii=False, sort_keys=True)

    logger.info(f"Wrote file {file_path}")

    return file_name


def find_temp_files(working_date: str):
    full_path = settings.OUTPUT_DIR / working_date
    return full_path.glob(settings.RAW_FILE_PATTERN)


def read_raw_logs(file_name) -> list:
    with open(file_name, encoding="utf8") as f:
        data = f.readlines()
        logger.info(f"Read {len(data)} lines from {file_name}")
    return data


def write_raw_logs(data: list, file_name: str, day: str) -> None:
    if not data:
        # prevents overwriting old files
        logger.error(f"No data. {file_name} not written")
        return
    full_path = settings.OUTPUT_DIR / day
    Path.mkdir(full_path, exist_ok=True, parents=True)
    path_file = full_path / file_name
    with open(path_file, "w", encoding="utf8") as f:
        for row in data:
            f.write(f"{row}\n")
    logger.info(f"Wrote {len(data)} to {path_file}")


def pickle_whitelists(pickle_this: list):
    file_name_path = cache_file_path / settings.WHITELIST_FILE
    with open(file_name_path, "wb") as f:
        pickle.dump(pickle_this, f, protocol=pickle.HIGHEST_PROTOCOL)


def unpickle_whitelist() -> set:
    file_name_path = cache_file_path / settings.WHITELIST_FILE
    with open(file_name_path, "rb") as f:
        return set(pickle.load(f))
