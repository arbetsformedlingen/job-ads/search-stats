import datetime
from collections import Counter
from loguru import logger

from search_trends import file_handler as fh
from search_trends.settings import PUBLIC
from search_trends import cleaner
from search_trends.query_string_parser import build_dict_from_raw_query_strings, check_data
from search_trends.taxonomy_translator import add_translations


def summarize_raw_files(files) -> list:
    all_raw_logs = []
    total = 0
    for f in files:
        raw_logs = fh.read_raw_logs(f)
        total += len(raw_logs)
        all_raw_logs.extend(raw_logs)
        logger.debug(f"{len(all_raw_logs)} logs from {f}")
    logger.info(f"Log rows: {total}")
    if total == 0:
        raise ValueError("No logs collected")
    return all_raw_logs


def count(raw_result: dict) -> dict:
    """
    Takes a dict with all fields and their values as lists
    summarizes results
    returns a new dictionary with summarized results
    """
    summarized_result = {}
    for key, val in raw_result.items():
        logger.info(f'{key}: {len(val)} objects')
        counted = Counter(val).most_common()
        # List contains [concept, count], sort by count descending and then by concept ascending
        # This is to make the output deterministic and thus makes tests easier
        summarized_result[key] = sorted(counted, key=lambda x: (-x[1], x[0]))
    return summarized_result


@logger.catch
def daily_file(working_date):
    start = datetime.datetime.now()
    raw_temp_files = fh.find_temp_files(working_date)
    all_raw_query_strings = summarize_raw_files(raw_temp_files)
    param_dict = build_dict_from_raw_query_strings(all_raw_query_strings)

    param_dict_with_checked_queries = check_data(param_dict)
    summarized_param_dict = count(param_dict_with_checked_queries)

    summarized_param_dict_labels = add_translations(summarized_param_dict)
    summarized_param_dict_labels['total_requests'] = len(all_raw_query_strings)
    final_param_dict = dict(summarized_param_dict_labels.items())
    write_output_files(final_param_dict, working_date)
    logger.info(f"finished in {datetime.datetime.now() - start}")


def write_output_files(params_dict, working_date):
    # write a complete file for analysis by JobTech
    fh.write_json_output_file(data=params_dict, suffix='daily', working_date=working_date)

    public_output_dict = cleaner.format_public(params_dict)

    # Public output file
    fh.write_json_output_file(data=public_output_dict, suffix='daily',
                              working_date=working_date, visibility=PUBLIC)
