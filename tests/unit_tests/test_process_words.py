from unittest.mock import patch
import pytest  # noqa: F401

from search_trends.process_words import (
    complex_search,
    find_matches,
    is_employer,
    is_organization_number,
    is_word_approved,
    search_word_stems,
)


def test_is_word_approved():
    # Act
    with patch("search_trends.process_words.whitelist", ["hobbit", "alv", "istari"]):
        result = is_word_approved("alv")

    # Assert
    assert result is True


@patch("search_trends.process_words.complex_search")
def test_is_word_approved_not_in_whitelist(mock_complex_search):
    # Arrange
    mock_complex_search.return_value = "complex-search-response"

    # Act
    with patch("search_trends.process_words.whitelist", ["hobbit", "alv", "istari"]):
        result = is_word_approved("orch")

    # Assert
    mock_complex_search.assert_called_once_with("orch")

    assert result == "complex-search-response"


@pytest.mark.parametrize("skola", ["hobbitskola", "hobbitskolan", "hobbitskolans"])
def test_check_school(skola):
    # Act
    with patch("search_trends.process_words.whitelist", ["hobbitskola", "alvskola"]):
        result = is_word_approved(skola)

    # Assert
    assert result is True


@pytest.mark.parametrize("skola", ["orchskola", "orchskolan", "orchskolans"])
def test_check_school_not_ok(skola):
    # Act
    with patch("search_trends.process_words.whitelist", ["hobbitskola", "alvskola"]):
        result = is_word_approved(skola)

    # Assert
    assert result is False


@patch("search_trends.process_words.check_school")
@patch("search_trends.process_words.is_percent")
@patch("search_trends.process_words.search_word_stems")
def test_complex_search_match_in_whitelist(
    mock_search_word_stems, mock_is_percent, mock_check_school
):
    # Arrange
    word = "nattanställning"
    whitelist = [
        "klimatlösningar",
        word,
        "auktorisationslösningar",
        "ortsanalyser",
        "medelhavsprodukter",
    ]

    # Act
    with patch("search_trends.process_words.whitelist", whitelist):
        result = complex_search(word)

    # Assert
    assert result is True
    mock_search_word_stems.assert_not_called()
    mock_is_percent.assert_not_called()
    mock_check_school.assert_not_called()


@patch("search_trends.process_words.settings.MINIMUM_WORD_LENGTH", 3)
@patch("search_trends.process_words.check_school")
@patch("search_trends.process_words.is_percent")
@patch("search_trends.process_words.search_word_stems")
def test_complex_search_with_short_words(
    mock_search_word_stems, mock_is_percent, mock_check_school
):
    # Arrange
    word = "en atomfysiker på en ö"
    whitelist = ["atomfysiker"]

    # Act
    with patch("search_trends.process_words.whitelist", whitelist):
        result = complex_search(word)

    # Assert
    assert result is True
    mock_search_word_stems.assert_not_called()
    mock_is_percent.assert_not_called()
    mock_check_school.assert_not_called()


@patch("search_trends.process_words.check_school")
@patch("search_trends.process_words.is_percent")
@patch("search_trends.process_words.search_word_stems")
def test_complex_search_match_in_word_stem(
    mock_search_word_stems, mock_is_percent, mock_check_school
):
    # Arrange
    word = "atomfysiker"
    whitelist = [
        "klimatlösningar",
        "auktorisationslösningar",
        "ortsanalyser",
        "medelhavsprodukter",
    ]

    mock_search_word_stems.return_value = True

    # Act
    with patch("search_trends.process_words.whitelist", whitelist):
        result = complex_search(word)

    # Assert
    assert result is True
    mock_search_word_stems.assert_called_once_with(word)
    mock_is_percent.assert_not_called()
    mock_check_school.assert_not_called()


@patch("search_trends.process_words.check_school")
@patch("search_trends.process_words.is_percent")
@patch("search_trends.process_words.search_word_stems")
def test_complex_search_match_in_is_percent(
    mock_search_word_stems, mock_is_percent, mock_check_school
):
    # Arrange
    word = "75%"
    whitelist = [
        "klimatlösningar",
        "auktorisationslösningar",
        "ortsanalyser",
        "medelhavsprodukter",
    ]

    mock_search_word_stems.return_value = False
    mock_is_percent.return_value = True

    # Act
    with patch("search_trends.process_words.whitelist", whitelist):
        result = complex_search(word)

    # Assert
    assert result is True
    mock_search_word_stems.assert_called_once_with(word)
    mock_is_percent.assert_called_once_with(word)
    mock_check_school.assert_not_called()


@patch("search_trends.process_words.check_school")
@patch("search_trends.process_words.is_percent")
@patch("search_trends.process_words.search_word_stems")
def test_complex_search_match_in_check_school(
    mock_search_word_stems, mock_is_percent, mock_check_school
):
    # Arrange
    word = "trollkarlsskola"
    whitelist = [
        "klimatlösningar",
        "auktorisationslösningar",
        "ortsanalyser",
        "medelhavsprodukter",
    ]

    mock_search_word_stems.return_value = False
    mock_is_percent.return_value = False
    mock_check_school.return_value = True

    # Act
    with patch("search_trends.process_words.whitelist", whitelist):
        result = complex_search(word)

    # Assert
    assert result is True
    mock_search_word_stems.assert_called_once_with(word)
    mock_is_percent.assert_not_called()
    mock_check_school.assert_called_once_with(word)


@pytest.mark.parametrize("word", ["hobbit", "hobbitar", "hobbitens", "alver"])
def test_search_word_stems(word):
    with patch("search_trends.process_words.whitelist", ["hobbit", "alv", "istari"]):
        result = search_word_stems(word)

    assert result is True


@pytest.mark.parametrize("word", ["troll", "trollet", "trollen", "orch"])
def test_search_word_stems_missing_in_whitelist(word):
    with patch("search_trends.process_words.whitelist", ["hobbit", "alv", "istari"]):
        result = search_word_stems(word)

    assert result is False


@patch("search_trends.process_words.settings")
def test_find_matches(mock_settings):
    # Arrange
    list_of_words = ["orch", "istari", "hobbit"]

    mock_settings.MINIMUM_COUNT = 0
    mock_settings.MINIMUM_WORD_LENGTH = 2

    # Act
    with patch("search_trends.process_words.whitelist", ["hobbit", "alv", "istari"]):
        result = find_matches(list_of_words)

    # Assert
    assert result == (["istari", "hobbit"], ["orch"], [])


@pytest.mark.parametrize(
    "word",
    ["5568500515", "polismyndigheten", "202100", "some-approved-word"],
)
@patch("search_trends.process_words.is_word_approved")
@patch("search_trends.process_words.is_organization_number")
@patch("search_trends.process_words.whitelisted_employers", ["polismyndigheten"])
def test_is_employer(mock_is_organization_number, mock_is_word_approved, word):
    # Arrange
    mock_is_organization_number.side_effect = lambda input: input == "5568500515"
    mock_is_word_approved.side_effect = lambda input: input == "some-approved-word"

    # Act
    result = is_employer(word)

    # Assert
    assert result is True


@patch("search_trends.process_words.is_word_approved")
@patch("search_trends.process_words.is_organization_number")
@patch("search_trends.process_words.whitelisted_employers", ["polismyndigheten"])
def test_is_employer_can_return_false(
    mock_is_organization_number, mock_is_word_approved
):
    # Arrange
    mock_is_organization_number.return_value = False
    mock_is_word_approved.return_value = False

    # Act
    result = is_employer("some-bogus-word")

    # Assert
    assert result is False


@pytest.mark.parametrize(
    "number",
    [
        "5568500515",
        "2021000225",
        "2120000449",
        "9696676312",
    ],
)
def test_is_organization_number_with_valid_input(number):
    # Act
    result = is_organization_number(number)

    # Assert
    assert result is True


@pytest.mark.parametrize(
    "number",
    [
        "0701234567",  # telephone number
        "0741234567",  # telephone number
        "2020ab2334",  # contains letters
        "5501234567",  # digit 3-4 < 20
        "2201234567",  # digit 3-4 < 20
        "22191234567",  # digit 3-4 < 20
        "240228-1234",  # with dash
        "polismyndigheten",  # not a number
        "5501011234",  # digit 3-4 must be > 20 otherwise it's a personal number
        "55685005150"  # too long
        "202402281234",  # too long
        "556850051",  # too short
        "202100",  # too short
        "1",  # too short
        "0",  # too short
        "",  # too short
        " "  # too short
        "          ",  # not a number
        None,  # wrong type
        False,  # wrong type
        None,  # wrong type
        [],  # wrong type
        {},  # wrong type
        5568500515,  # only string is allowed
    ],
)
def test_is_organization_number_with_invalid_input(number):
    # Act
    result = is_organization_number(number)

    # Assert
    assert result is False
