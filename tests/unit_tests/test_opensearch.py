import pytest  # noqa: F401
from unittest.mock import patch

from search_trends.opensearch import create_opensearch_client, scan


@patch("search_trends.opensearch.logger")
@patch("search_trends.opensearch.OpenSearch")
@patch("search_trends.opensearch.settings")
def test_create_opensearch_client(mock_settings, MockOpenSearch, mock_logger):
    # Arrange
    mock_settings.OPENSEARCH_HOST = "test-host"
    mock_settings.OPENSEARCH_PORT = "test-port"
    mock_settings.OPENSEARCH_CAFILE = "test-cafile"
    mock_settings.OPENSEARCH_USER = "test-user"
    mock_settings.OPENSEARCH_PWD = "test-pwd"
    mock_settings.OPENSEARCH_TIMEOUT = "test-timeout"

    # Act
    client = create_opensearch_client()

    # Assert
    assert client == MockOpenSearch.return_value

    MockOpenSearch.assert_called_once_with(
        [{"host": "test-host", "port": "test-port"}],
        ca_certs="test-cafile",
        use_ssl=True,
        ssl_assert_hostname=False,
        verify_certs=False,
        ssl_show_warn=False,
        http_auth=("test-user", "test-pwd"),
        timeout="test-timeout",
    )

    # Check that info was logged
    mock_logger.info.assert_called_once_with(
        MockOpenSearch.return_value.info.return_value
    )


@patch("search_trends.opensearch.settings")
@patch("search_trends.opensearch.opensearchpy.helpers")
@patch("search_trends.opensearch.create_opensearch_client")
def test_scan(mock_create_opensearch_client, mock_helpers, mock_settings):
    # Arrange
    mock_create_opensearch_client.return_value = "test-client"
    mock_settings.OPENSEARCH_INDEX_PREFIX = "test-index-prefix"
    mock_helpers.scan.return_value = "test-scan-result"

    # Act
    result = scan("test-query")

    # Assert
    mock_helpers.scan.assert_called_once_with(
        "test-client",
        index="test-index-prefix",
        query="test-query",
        size=10000,
        scroll="15m",
    )

    assert result == "test-scan-result"
