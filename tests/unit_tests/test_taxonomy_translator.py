from unittest.mock import patch
import pytest  # noqa: F401

from search_trends.taxonomy_translator import add_translations, translate_list


def test_translate_list():
    # Arrange
    list_of_results = [
        ["concept-a", 32],
        ["concept-b", 12],
        ["concept-c", 5],
        ["-concept-a", 4],
    ]
    taxonomy_pairs = {
        "concept-a": "Concept A",
        "concept-b": "Concept B",
        "concept-c": "Concept C",
    }

    # Act
    results = translate_list(list_of_results, taxonomy_pairs)

    # Assert
    assert results == [
        ("Concept A", 32),
        ("Concept B", 12),
        ("Concept C", 5),
        ("-Concept A", 4),
    ]


def test_translate_list_with_empty_list():
    # Act
    results = translate_list([], {})

    # Assert
    assert results == []


def test_translate_list_missing_translations():
    # Arrange
    list_of_results = [
        ["concept-a", 32],
        ["concept-b", 12],
        ["concept-c", 5],
        ["-concept-a", 4],
    ]
    taxonomy_pairs = {
        "concept-b": "Concept B",
    }

    # Act
    results = translate_list(list_of_results, taxonomy_pairs)

    # Assert
    assert results == [
        ("concept-a", 32),
        ("Concept B", 12),
        ("concept-c", 5),
        ("-concept-a", 4),
    ]


def test_add_translations():
    # Arrange
    summarized_dict = {
        "municipality": [["Namm_SpC_RPG", 34]],
        "occupation-group": [["MrcX_RDn_N53", 23], ["ENV6_BNa_brP", 6]],
        "language": [["NVxJ_hLg_TYS", 11]],
    }

    # Act
    result = add_translations(summarized_dict)

    # Assert
    assert result == {
        "municipality": [["Namm_SpC_RPG", 34]],
        "municipality-label": [("Tranås", 34)],
        "occupation-group": [["MrcX_RDn_N53", 23], ["ENV6_BNa_brP", 6]],
        "occupation-group-label": [
            ("Truckförare", 23),
            ("Maskinoperatörer, gummiindustri", 6),
        ],
        "language": [["NVxJ_hLg_TYS", 11]],
        "language-label": [("Engelska", 11)],
    }


def test_add_translations_full_dict():
    # Arrange
    summarized_dict = {
        "municipality": [["Namm_SpC_RPG", 34], ["some-unknown-municipality", 2]],
        "region": [["MtbE_xWT_eMi", 23], ["EVVp_h6U_GSZ", 4]],
        "country": [["SntG_Zrn_SR3", 11], ["i46j_HmG_v64", 4593]],
        "occupation-name": [["LE93_Yjz_WE3", 4], ["EUbK_SKX_mHa", 5]],
        "occupation-group": [["some-unknown-occupation-group", 8], ["bwnk_nv2_1cR", 5]],
        "occupation-field": [
            ["ScKy_FHB_7wT", 7823],
            ["some-unknown-occupation-field", 6],
        ],
        "occupation-collection": [["8too_bEs_7NU", 21]],
        "employment-type": [["sTu5_NBQ_udq", 34], ["EBhX_Qm2_8eX", 12]],
        "worktime-extent": [["6YE1_gAC_R2G", 234]],
        "language": [["NVxJ_hLg_TYS", 34], ["some-unknown-language", 7]],
    }

    # Act
    result = add_translations(summarized_dict)

    # Assert
    assert result == {
        "municipality": [["Namm_SpC_RPG", 34], ["some-unknown-municipality", 2]],
        "municipality-label": [("Tranås", 34), ("some-unknown-municipality", 2)],
        "region": [["MtbE_xWT_eMi", 23], ["EVVp_h6U_GSZ", 4]],
        "region-label": [("Jönköpings län", 23), ("Värmlands län", 4)],
        "country": [["SntG_Zrn_SR3", 11], ["i46j_HmG_v64", 4593]],
        "country-label": [("Bangladesh", 11), ("Sverige", 4593)],
        "occupation-name": [["LE93_Yjz_WE3", 4], ["EUbK_SKX_mHa", 5]],
        "occupation-name-label": [("Tårtmakare", 4), ("Kraftverksoperatör", 5)],
        "occupation-group": [["some-unknown-occupation-group", 8], ["bwnk_nv2_1cR", 5]],
        "occupation-group-label": [
            ("some-unknown-occupation-group", 8),
            ("Ramppersonal, flyttkarlar och varupåfyllare m.fl.", 5),
        ],
        "occupation-field": [
            ["ScKy_FHB_7wT", 7823],
            ["some-unknown-occupation-field", 6],
        ],
        "occupation-field-label": [
            ("Hotell, restaurang, storhushåll", 7823),
            ("some-unknown-occupation-field", 6),
        ],
        "occupation-collection": [["8too_bEs_7NU", 21]],
        "occupation-collection-label": [("Kultur", 21)],
        "employment-type": [["sTu5_NBQ_udq", 34], ["EBhX_Qm2_8eX", 12]],
        "employment-type-label": [
            ("Tidsbegränsad anställning", 34),
            ("Säsongsanställning", 12),
        ],
        "worktime-extent": [["6YE1_gAC_R2G", 234]],
        "worktime-extent-label": [("Heltid", 234)],
        "language": [["NVxJ_hLg_TYS", 34], ["some-unknown-language", 7]],
        "language-label": [("Engelska", 34), ("some-unknown-language", 7)],
    }


def test_add_translations_with_empty_dict():
    # Arrange
    summarized_dict = {}

    # Act
    result = add_translations(summarized_dict)

    # Assert
    assert result == {}
