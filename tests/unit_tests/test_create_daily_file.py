import pytest  # noqa: F401
from unittest.mock import call, patch


from search_trends.create_daily_file import (
    count,
    daily_file,
    summarize_raw_files,
    write_output_files,
)


def test_summarize_raw_files_with_empty_list():
    with pytest.raises(ValueError) as e:
        summarize_raw_files([])


@patch("search_trends.create_daily_file.fh.read_raw_logs")
def test_summarizes_raw_files(mock_read_raw_logs):
    # Arrange
    files = ["file1", "file2", "file3"]

    mock_read_raw_logs.side_effect = [
        ["log-1", "log-2", "log-3"],
        ["log-4", "log-5"],
        ["log-6", "log-7", "log-8", "log-9"],
    ]

    # Act
    logs = summarize_raw_files(files)

    # Assert
    assert mock_read_raw_logs.call_count == 3
    assert logs == [
        "log-1",
        "log-2",
        "log-3",
        "log-4",
        "log-5",
        "log-6",
        "log-7",
        "log-8",
        "log-9",
    ]


def test_count_empty_dict():
    result = count({})

    assert result == {}


def test_count():
    result = count(
        {
            "x": ["b", "a", "b", "c"],
            "y": ["a", "b", "a"],
            "z": ["a", "d", "d", "b", "d"],
        }
    )

    assert result == {
        "x": [("b", 2), ("a", 1), ("c", 1)],
        "y": [("a", 2), ("b", 1)],
        "z": [("d", 3), ("a", 1), ("b", 1)],
    }


@patch("search_trends.create_daily_file.write_output_files")
@patch("search_trends.create_daily_file.add_translations")
@patch("search_trends.create_daily_file.count")
@patch("search_trends.create_daily_file.check_data")
@patch("search_trends.create_daily_file.build_dict_from_raw_query_strings")
@patch("search_trends.create_daily_file.summarize_raw_files")
@patch("search_trends.create_daily_file.fh.find_temp_files")
def test_daily_file(
    mock_find_temp_files,
    mock_summarize_raw_files,
    mock_build_dict_from_raw_query_strings,
    mock_check_data,
    mock_count,
    mock_add_translations,
    mock_write_output_files,
):
    # Arrange
    date = "2024-05-02"

    mock_find_temp_files.return_value = "test-temp-files"
    mock_summarize_raw_files.return_value = ["log-1", "log-2", "log-3"]
    mock_build_dict_from_raw_query_strings.return_value = (
        "test-dict-from-raw-query-strings"
    )
    mock_check_data.return_value = "test-checked-data"
    mock_count.return_value = "test-counted-data"
    mock_add_translations.return_value = {"key-1": "value-1", "key-2": "value-2"}

    # Act
    daily_file(date)

    # Assert
    mock_find_temp_files.assert_called_once_with(date)
    mock_summarize_raw_files.assert_called_once_with("test-temp-files")
    mock_build_dict_from_raw_query_strings.assert_called_once_with(
        ["log-1", "log-2", "log-3"]
    )
    mock_check_data.assert_called_once_with("test-dict-from-raw-query-strings")
    mock_count.assert_called_once_with("test-checked-data")
    mock_add_translations.assert_called_once_with("test-counted-data")

    mock_write_output_files.assert_called_once_with(
        {"key-1": "value-1", "key-2": "value-2", "total_requests": 3}, date
    )


@patch("search_trends.create_daily_file.cleaner.format_public")
@patch("search_trends.create_daily_file.fh.write_json_output_file")
def test_write_output_files(mock_write_json_output_file, mock_format_public):
    # Arrange
    date = "2024-06-04"

    mock_format_public.return_value = "test-cleaned-data"

    # Act
    write_output_files("test-data", date)

    # Assert

    mock_write_json_output_file.assert_has_calls(
        [
            call(data="test-data", suffix="daily", working_date=date),
            call(
                data="test-cleaned-data",
                suffix="daily",
                working_date=date,
                visibility="public",
            ),
        ]
    )

    mock_format_public.assert_called_once_with("test-data")
