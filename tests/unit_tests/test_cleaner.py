import pytest  # noqa: F401
from unittest.mock import patch

from search_trends.cleaner import (
    clean_word,
    decode_url_formatting,
    format_public,
    is_percent,
)


@patch("search_trends.cleaner.urllib")
def test_decode_url_formatting(mock_urllib):
    # Arrange
    mock_urllib.parse.unquote.return_value = "unquoted-word"

    # Act
    result = decode_url_formatting("quoted-word")

    # Assert
    assert result == "unquoted-word"

    mock_urllib.parse.unquote.assert_called_once_with("quoted-word")


@pytest.mark.parametrize(
    "word, expected",
    [
        ("skåne ", "skåne"),
        (" skåne ", "skåne"),
        (" skåne", "skåne"),
        (" Skåne", "skåne"),
        (" Skåne!..", "skåne"),
        (" Skåne!..", "skåne"),
        (" Skåne/halland!..", "skåne halland"),
        (" Skåne / halland!..", "skåne   halland"),
        ("Åã¥å", "ååå"),
        ("äã¤Ä", "äää"),
        ("Öã¶ö", "ööö"),
        ("Öã–Ö", "ööö"),
    ],
)
def test_word_cleaner(word, expected):
    assert clean_word(word) == expected


@patch("search_trends.cleaner.logger")
@patch("search_trends.cleaner.urllib")
def test_clean_word_handles_exception(mock_urllib, mock_logger):
    # Arrange
    mock_urllib.parse.unquote.side_effect = AttributeError("test error message")

    # Act

    clean_word("some-word")

    # Assert
    mock_urllib.parse.unquote.assert_called_once_with("some-word")
    mock_logger.error.assert_called_once_with("some-word - test error message")


@pytest.mark.parametrize(
    "char",
    [
        "+",
        ",",
        "-",
        ";",
        ":",
        "&",
        "?",
        "!",
        ".",
        "*",
        "/",
        "$",
        "\\",
        "(",
        ")",
        '"',
        "'",
        "<",
        ">",
        "„",
        "+",
        "*",
    ],
)
def test_remove_char(char):
    test_string = f"abc{char}123"
    assert clean_word(test_string) == "abc 123"


@pytest.mark.parametrize("char", ["¹", "𝕜"])
def test_alphanumeric_ok(char):
    test_string = f"abc{char}123"
    assert clean_word(test_string) == "abc 123"


@pytest.mark.parametrize(
    "word, expected",
    [
        ("5568500515", "5568500515"),
        ("%22Polismyndigheten%22", "polismyndigheten"),
        (
            "%22Karolinska%20Universitetssjukhuset%22",
            "karolinska universitetssjukhuset",
        ),
    ],
)
def test_employer_formatting(word, expected):
    result = clean_word(word)
    assert result == expected


@pytest.mark.parametrize(
    "word, expected",
    [
        ("23", False),
        ("23%", True),
        ("100%", True),
        ("1000%", False),
        ("%", False),
        ("75 %", False),
    ],
)
def test_percent(word, expected):
    assert is_percent(word) == expected


def test_format_public():
    """
    Test that only defined fields are included in the output
    """
    data = {
        "q_approved": ["ok", "words"],
        "q_internal_unapproved": ["not", "approved"],
        "q_internal_not_checked": ["not", "checked"],
        "something": [123, 456],
        "region": ["stockholm"],
    }

    formatted_result = format_public(data)
    assert formatted_result == {"q_approved": ["ok", "words"], "region": ["stockholm"]}
