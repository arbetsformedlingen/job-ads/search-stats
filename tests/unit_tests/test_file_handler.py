import pytest  # noqa: F401
import pickle
from pathlib import Path
from unittest.mock import mock_open, patch

from search_trends.file_handler import (
    find_temp_files,
    pickle_whitelists,
    read_raw_logs,
    unpickle_whitelist,
    write_json_output_file,
    write_raw_logs,
)


@patch("search_trends.file_handler.settings")
@patch("search_trends.file_handler.Path.mkdir")
@patch("json.dump")
@patch("builtins.open")
def test_write_json_output_file(mock_open, mock_json_dump, mock_mkdir, mock_settings):
    # Arrange
    mock_settings.OUTPUT_DIR = Path("/tmp/test_output_dir")
    mock_settings.SERVICE_NAME = "test_service_name"

    test_data = {"test": "data"}
    working_date = "2022-05-23"
    visibility = "INTERNAL"

    # Act
    result = write_json_output_file(test_data, "test", working_date, visibility)

    # Assert
    # Check that the folder is created
    mock_mkdir.assert_called_once_with(exist_ok=True, parents=True)

    # Check that correct file is opened for writing
    expected_file_name = "test_service_name-2022-05-23-test-INTERNAL.json"

    expected_full_path = mock_settings.OUTPUT_DIR / expected_file_name
    mock_open.assert_called_once_with(expected_full_path, "w", encoding="utf8")

    # Check that the data is written to the file
    mock_json_dump.assert_called_once_with(
        test_data,
        mock_open.return_value.__enter__.return_value,
        ensure_ascii=False,
        sort_keys=True,
    )

    # Check that filename is returned
    assert result == expected_file_name


@patch("search_trends.file_handler.settings")
def test_find_temp_files(mock_settings, tmp_path):
    # Arrange
    working_date = "2024-07-02"

    (tmp_path / working_date).mkdir(parents=True)
    (tmp_path / working_date / "test_file1-RAW.txt").touch()
    (tmp_path / working_date / "test_file2-RAW.txt").touch()

    mock_settings.OUTPUT_DIR = tmp_path
    mock_settings.RAW_FILE_PATTERN = "*-RAW.txt"

    # Act
    result = find_temp_files(working_date)

    # Assert
    assert list(result) == [
        tmp_path / "2024-07-02/test_file1-RAW.txt",
        tmp_path / "2024-07-02/test_file2-RAW.txt",
    ]


def test_read_raw_logs():
    # Arrange
    file_name = "test_file.txt"

    test_data = "test data\nwith second line\n"

    # Act
    with patch("builtins.open", mock_open(read_data=test_data)) as m:
        result = read_raw_logs(file_name)

    # Assert
    m.assert_called_once_with(file_name, encoding="utf8")

    assert result == ["test data\n", "with second line\n"]


@patch("search_trends.file_handler.settings")
def test_write_raw_logs(mock_settings, tmp_path):
    # Arrange
    data = ["test data", "with second line"]
    file_name = "test_file.txt"
    day = "2024-07-02"

    mock_settings.OUTPUT_DIR = tmp_path

    # Act
    write_raw_logs(data, file_name, day)

    # Assert
    with open(tmp_path / "2024-07-02/test_file.txt", "r") as f:
        content = f.read()
        assert content == "test data\nwith second line\n"


@patch("builtins.open")
def test_write_raw_logs_doesnt_open_file_without_data(mock_open):
    # Act
    write_raw_logs([], "test_file.txt", "2024-07-02")

    # Assert
    assert mock_open.call_count == 0


@patch("search_trends.file_handler.settings")
@patch("search_trends.file_handler.pickle.dump")
@patch("builtins.open")
def test_pickle_whitelist(mock_open, mock_pickle_dump, mock_settings):
    # Arrange
    pickle_this = ["test", "data"]

    mock_settings.WHITELIST_FILE = "whitelist.pkl"

    # Act
    with patch("search_trends.file_handler.cache_file_path", Path("cache")):
        pickle_whitelists(pickle_this)

    # Assert
    mock_open.assert_called_once_with(Path("cache/whitelist.pkl"), "wb")

    mock_pickle_dump.assert_called_once_with(
        pickle_this,
        mock_open.return_value.__enter__.return_value,
        protocol=pickle.HIGHEST_PROTOCOL,
    )


@patch("search_trends.file_handler.settings")
@patch("search_trends.file_handler.pickle.load")
@patch("builtins.open")
def test_unpickle_whitelist(mock_open, mock_pickle_load, mock_settings):
    # Arrange
    mock_settings.WHITELIST_FILE = "whitelist.pkl"

    mock_pickle_load.return_value = ["test", "data"]

    # Act
    with patch("search_trends.file_handler.cache_file_path", Path("cache")):
        data = unpickle_whitelist()

    # Assert
    mock_open.assert_called_once_with(Path("cache/whitelist.pkl"), "rb")

    mock_pickle_load.assert_called_once_with(
        mock_open.return_value.__enter__.return_value,
    )

    assert data == {"test", "data"}  # set
