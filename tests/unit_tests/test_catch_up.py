import pytest  # noqa: F401
from datetime import datetime
from unittest.mock import call, patch

from search_trends.catch_up import get_last_week


@patch("search_trends.create_temp_files.datetime")
@patch("search_trends.catch_up.create_temp_files_from_whole_day")
def test_get_last_week(mock_create_temp_files_from_whole_day, mock_datetime):
    test_time = datetime(2024, 3, 4, 13, 20, 34)
    mock_datetime.now.return_value = test_time

    get_last_week()

    assert mock_create_temp_files_from_whole_day.call_count == 7

    mock_create_temp_files_from_whole_day.assert_has_calls(
        [
            call("2024-03-03"),
            call("2024-03-02"),
            call("2024-03-01"),
            call("2024-02-29"),
            call("2024-02-28"),
            call("2024-02-27"),
        ]
    )
