from unittest.mock import patch
import pytest  # noqa: F401

from search_trends.main import is_date, main


@patch("search_trends.main.parse")
def test_is_date_calls_parse(mock_parse):
    # Act
    result = is_date("2024-03-04")

    # Assert
    assert result is True

    mock_parse.assert_called_once_with("2024-03-04", fuzzy=False)


@patch("search_trends.main.parse")
def test_is_date_returns_false_on_ValueError(mock_parse):
    # Arrange
    mock_parse.side_effect = ValueError

    # Act
    result = is_date("some-input")

    # Assert
    assert result is False


@patch("search_trends.main.parse")
def test_is_date_returns_false_on_TypeError(mock_parse):
    # Arrange
    mock_parse.side_effect = TypeError

    # Act
    result = is_date("some-input")

    # Assert
    assert result is False


@patch("search_trends.main.daily_file")
@patch("search_trends.main.create_temp_files_from_whole_day")
@patch("search_trends.main.is_date")
@patch("search_trends.main.settings")
def test_main_with_date_from_settings(
    mock_settings, mock_is_date, mock_create_temp_files_from_whole_day, mock_daily_file
):
    # Arrange
    date = "2024-03-04"

    mock_settings.DATE = date
    mock_is_date.return_value = True

    # Act
    main()

    # Assert
    mock_is_date.assert_called_once_with(date)

    mock_create_temp_files_from_whole_day.assert_called_once_with(date)

    mock_daily_file.assert_called_once_with(date)


@patch("search_trends.main.daily_file")
@patch("search_trends.main.create_temp_files_from_whole_day")
@patch("search_trends.main.days_ago")
@patch("search_trends.main.is_date")
@patch("search_trends.main.settings")
def test_main_with_date_from_days_ago_function(
    mock_settings,
    mock_is_date,
    mock_days_ago,
    mock_create_temp_files_from_whole_day,
    mock_daily_file,
):
    # Arrange
    date = "2024-03-04"

    mock_settings.DATE = None
    mock_is_date.return_value = False
    mock_days_ago.return_value = date

    # Act
    main()

    # Assert
    mock_is_date.assert_called_once_with(None)
    mock_days_ago.assert_called_once()

    mock_create_temp_files_from_whole_day.assert_called_once_with(date)

    mock_daily_file.assert_called_once_with(date)
