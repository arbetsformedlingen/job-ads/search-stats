from unittest.mock import patch
import pytest  # noqa: F401
from search_trends.query_string_parser import (
    build_dict_from_raw_query_strings,
    check_data,
    check_employer,
    parse_scan_result,
)


def test_parse_scan_result():
    # Arrange
    scan_result = [
        {"fields": {"calamari.request.query_string": ["test-query-1"]}},
        {"fields": {"some-other-key": "some-other-value"}},
        {"fields": {"calamari.request.query_string": ["test-query-2"]}},
    ]

    # Act
    result = parse_scan_result(scan_result)

    # Assert
    assert result == ["test-query-1", "test-query-2"]


def test_parse_scan_result_with_empty_list():
    # Act
    result = parse_scan_result([])

    # Assert
    assert result == []


def test_build_dict_from_raw_query_strings():
    # Arrange
    raw_query_strings = [
        "a=xyz&q=abc",
        "a=xyz&b=uvw",
        "q=xyz&a=abc",
        "q=uvw&q=xyz",
    ]

    # Act
    result = build_dict_from_raw_query_strings(raw_query_strings)

    # Assert
    assert result == {
        "a": ["xyz", "xyz", "abc"],
        "b": ["uvw"],
        "q": ["abc", "xyz", "uvw", "xyz"],
    }


def test_build_dict_from_raw_query_strings_with_empty_list():
    # Act
    result = build_dict_from_raw_query_strings([])

    # Assert
    assert result == {}


def test_check_data():
    ok_word = "bagare"
    not_ok_word = "unapproved"
    not_checked_word = "1"

    data = {
        "q": [ok_word, not_ok_word, not_checked_word],
        "something_else": 123,
        "employer": [],
    }

    result = check_data(data)

    assert result["q_approved"] == [ok_word]
    assert result["q_internal_unapproved"] == [not_ok_word]
    assert result["q_internal_not_checked"] == [not_checked_word]


@patch("search_trends.query_string_parser.process_words.is_employer")
@patch("search_trends.query_string_parser.process_words.clean_word")
def test_check_employer(mock_clean_word, mock_is_employer):
    # Arrange
    employers = ["employer-1", "employer-2", "not-employer", "employer-3"]

    mock_clean_word.side_effect = lambda x: x
    mock_is_employer.side_effect = lambda x: x.startswith("employer")

    # Act
    result = check_employer(employers)

    # Assert
    assert result == ["employer-1", "employer-2", "employer-3"]
