import pytest  # noqa: F401
from datetime import datetime
from unittest.mock import call, patch

from search_trends.create_temp_files import days_ago, create_temp_files_from_whole_day


@patch("search_trends.create_temp_files.datetime")
def test_days_ago_without_argument(mock_datetime):
    test_time = datetime(2024, 6, 12, 13, 20, 34)
    mock_datetime.now.return_value = test_time

    date = days_ago()

    assert date == "2024-06-11"


@pytest.mark.parametrize(
    "days_in_the_past, expected",
    [
        (0, "2024-06-12"),
        (3, "2024-06-09"),
        (7, "2024-06-05"),
    ],
)
@patch("search_trends.create_temp_files.datetime")
def test_days_ago_with_argument(mock_datetime, days_in_the_past, expected):
    test_time = datetime(2024, 6, 12, 13, 20, 34)
    mock_datetime.now.return_value = test_time

    date = days_ago(days_in_the_past)

    assert date == expected


@patch("search_trends.create_temp_files.write_raw_logs")
@patch("search_trends.create_temp_files.parse_scan_result")
@patch("search_trends.create_temp_files.scan")
@patch("search_trends.create_temp_files.log_query")
def test_create_temp_files_from_whole_day(
    mock_log_query, mock_scan, mock_parse_scan_result, mock_write_raw_logs
):
    # Arrange
    date = "2024-03-04"

    queries = [f"test-query-{i}" for i in range(24)]
    mock_log_query.side_effect = queries

    scan_results = [f"test-scan-result-{i}" for i in range(24)]
    mock_scan.side_effect = scan_results

    raw_results = [f"test-raw-result-{i}" for i in range(24)]
    mock_parse_scan_result.side_effect = raw_results

    # Act
    create_temp_files_from_whole_day(date)

    # Assert

    # Check for all 24 calls to query
    assert mock_log_query.call_count == 24
    mock_log_query.assert_has_calls(
        [
            call(
                f"2024-03-04T{i:02d}:00:00.000Z",
                f"2024-03-04T{i:02d}:59:59.999Z",
                "jobsearch.api.jobtechdev.se",
            )
            for i in range(24)
        ]
    )

    # Check that the received query is used in scan calls
    assert mock_scan.call_count == 24
    mock_scan.assert_has_calls([call(query) for query in queries])

    # Check that the scan results are passed to parse_scan_results
    assert mock_parse_scan_result.call_count == 24
    mock_parse_scan_result.assert_has_calls(
        [call(scan_result) for scan_result in scan_results]
    )

    # Check that the parsed results are written to files
    assert mock_write_raw_logs.call_count == 24
    mock_write_raw_logs.assert_has_calls(
        [
            call(f"test-raw-result-{i}", file_name=f"{date}T{i:02d}Z-RAW.txt", day=date)
            for i in range(24)
        ]
    )
