from pprint import pprint
import pytest  # noqa: F401
import json
import os
from unittest.mock import call, patch
from search_trends.main import main


# @patch("search_trends.file_handler.settings")
@patch("search_trends.create_temp_files.scan")
def test_run_main(mock_scan, tmp_path, snapshot):
    # Arrange
    scan_results = json.load(open("tests/fixtures/scan_results.json"))
    mock_scan.side_effect = scan_results

    # mock_settings_in_filehandler.OUTPUT_DIR = tmp_path

    # Act
    with patch("search_trends.file_handler.settings.OUTPUT_DIR", tmp_path):
        main()

    # Assert

    # Check that 24 (one for each hour) calls were made to OpenSearch
    mock_scan.assert_has_calls(
        [
            call(
                {
                    "fields": [{"field": "calamari.request.query_string"}],
                    "query": {
                        "bool": {
                            "filter": [
                                {
                                    "range": {
                                        "@timestamp": {
                                            "format": "strict_date_optional_time",
                                            "gte": f"2024-07-02T{i:02d}:00:00.000Z",
                                            "lte": f"2024-07-02T{i:02d}:59:59.999Z",
                                        }
                                    }
                                },
                                {"exists": {"field": "calamari.request.query_string"}},
                                {"match_phrase": {"calamari.request.path": "/search"}},
                                {
                                    "match_phrase": {
                                        "calamari.host": "jobsearch.api.jobtechdev.se"
                                    }
                                },
                            ],
                            "must_not": [
                                {"match_phrase": {"url.query": "?region=01&limit=0"}},
                                {"match_phrase": {"url.query": "?offset=0&limit=0"}},
                                {
                                    "match_phrase": {
                                        "url.query": "?offset=0&limit=0&show-count=false"
                                    }
                                },
                            ],
                        }
                    },
                }
            )
            for i in range(24)
        ],
        any_order=True,
    )

    # Check that the output files were written
    assert os.path.exists(tmp_path / "jobsearch-2024-07-02-daily-INTERNAL.json")
    assert os.path.exists(tmp_path / "jobsearch-2024-07-02-daily-public.json")

    # Check contents of INTERNAL file
    data = json.load(open(tmp_path / "jobsearch-2024-07-02-daily-INTERNAL.json"))
    # expected = json.load(open("tests/fixtures/expected-internal.json"))
    assert data == snapshot(name="internal")

    # Check contents of public file
    data = json.load(open(tmp_path / "jobsearch-2024-07-02-daily-public.json"))
    # expected = json.load(open("tests/fixtures/expected-public.json"))
    assert data == snapshot(name="public")
