# Search-Trends

Summarizes daily usage of JobSearch API from its request logs.  
Once a day, it collects logs from the OpenSearch-instance that saves all requests to all apis.  
The logs are summarized, personally identifiable information (PII) is removed and the result is saved as a public json file to data.jobtechdev.se
Additionally, a file for internal use by the development team (which may contain PII) is saved to a Minio bucket


## Why?

- Get to know how the api:s are being used.
- Make it public what the people searching for jobs are searching for
- Make it possible to see changes and trends over time.

## Summarize logs

- Collected with OpenSearch queries
- Yesterday's logs are collected and a temp file is created for every hour. This is done to reduce the amount of data collected in a query.
- Logs that come from our own systems that checks if the system is alive are excluded
- Hourly temp files are summarized and checked
- Result is written to json files

### Output format

Searches are split into their parameters. Right now we only present the parts, not how they are combined
All parameters are sorted by frequency.
concept ids are translated and copied into separate entries, so that a human-readable name is used

For details on the fields, look in `docs/fields.md`

### Limitations

Some free text searches are removed for GDPR reasons (they include names, telephone numbers and similar information).  
Generally, these are not frequent and their omission shouldn't affect any statistics or trends.
Complete search combinations are not included, only the parts

## Output file

JSON-format, each parameter is a list of tuples with the search parameter and its frequency.
e.g. the most common free text searches:
`"q_approved": [["stockholms län", 27261], ["undersköterska", 26462], ["personlig assistent", 24078], ["lagerarbetare", 20737],`
"stockholms län" was used in free text searches 27261 times during this day.

When concept ids are used, a translation is also provided. e.g
`"region": [["CifL_Rzy_Mku", 150831], ["CaRE_1nn_cSU", 73236], ["zdoY_6u5_Krt", 48812],`
and its translated companion which has the suffix `-label`:
`"region-label": [["Stockholms län", 150831], ["Skåne län", 73236], ["Västra Götalands län", 48812], `

## Running the program

Read `docs/developer_setup.md`

## Understanding the data

Read `docs/understanding_output_data.md`

## Documentation

More info is found in the `docs` folder
