import sys
from loguru import logger
import search_trends.main as main

if __name__ == '__main__':
    try:
        main.main()
    except Exception as e:
        logger.exception(e)
        sys.exit(-1)
    else:
        logger.info("Finished")
